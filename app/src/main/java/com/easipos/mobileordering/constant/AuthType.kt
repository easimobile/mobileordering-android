package com.easipos.mobileordering.constant

object AuthType {

    const val FACEBOOK = "FACEBOOK"
    const val GOOGLE = "GOOGLE"
    const val PASSWORD = "PASSWORD"
}