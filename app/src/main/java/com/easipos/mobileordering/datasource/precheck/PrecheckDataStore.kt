package com.easipos.mobileordering.datasource.precheck

import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckDataStore {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
