package com.easipos.mobileordering.datasource

import android.app.Application
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.datasource.auth.AuthDataSource
import com.easipos.mobileordering.datasource.auth.AuthDataStore
import com.easipos.mobileordering.datasource.notification.NotificationDataSource
import com.easipos.mobileordering.datasource.notification.NotificationDataStore
import com.easipos.mobileordering.datasource.ordering.OrderingDataSource
import com.easipos.mobileordering.datasource.ordering.OrderingDataStore
import com.easipos.mobileordering.datasource.precheck.PrecheckDataSource
import com.easipos.mobileordering.datasource.precheck.PrecheckDataStore
import com.easipos.mobileordering.datasource.user.UserDataSource
import com.easipos.mobileordering.datasource.user.UserDataStore
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.room.RoomService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataFactory @Inject constructor(private val application: Application,
                                      private val api: ApiService,
                                      private val roomService: RoomService,
                                      private val threadExecutor: ThreadExecutor,
                                      private val postExecutionThread: PostExecutionThread) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api, threadExecutor, postExecutionThread)

    fun createAuthDataSource(): AuthDataStore =
        AuthDataSource(api, threadExecutor, postExecutionThread)

    fun createUserDataSource(): UserDataStore =
        UserDataSource(api, threadExecutor, postExecutionThread)

    fun createOrderingDataSource(): OrderingDataStore =
        OrderingDataSource(application, api, roomService, threadExecutor, postExecutionThread)

    fun createNotificationDataSource(): NotificationDataStore =
        NotificationDataSource(api, threadExecutor, postExecutionThread)
}
