package com.easipos.mobileordering.datasource.ordering

import android.app.Application
import android.os.AsyncTask
import com.easipos.mobileordering.api.misc.EmptySingleObserverRetrofit
import com.easipos.mobileordering.api.misc.SingleObserverRetrofit
import com.easipos.mobileordering.api.requests.ordering.*
import com.easipos.mobileordering.api.responses.ordering.MenuResponseModel
import com.easipos.mobileordering.api.responses.ordering.OrderItemResponseModel
import com.easipos.mobileordering.api.responses.ordering.ProductResponseModel
import com.easipos.mobileordering.api.responses.ordering.RemarkResponseModel
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.mapper.ordering.MenuMapper
import com.easipos.mobileordering.mapper.ordering.ProductMapper
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Menu
import com.easipos.mobileordering.models.Product
import com.easipos.mobileordering.models.Remark
import com.easipos.mobileordering.room.RoomService
import com.easipos.mobileordering.use_cases.complete
import com.easipos.mobileordering.use_cases.error
import com.easipos.mobileordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderingDataSource @Inject constructor(private val application: Application,
                                             private val api: ApiService,
                                             private val roomService: RoomService,
                                             private val threadExecutor: ThreadExecutor,
                                             private val postExecutionThread: PostExecutionThread
) :
    OrderingDataStore {

    override fun getMenu(menuMapper: MenuMapper): Single<List<Menu>> =
        Single.create { emitter ->
            api.getMenu()
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<MenuResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<MenuResponseModel>) {
                        Logger.i("Menu retrieved")

                        emitter.success(menuMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getNewProducts(model: GetNewProductsRequestModel, productMapper: ProductMapper): Single<List<Product>> =
        Single.create { emitter ->
            api.getNewProducts(model.deptCd)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<ProductResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<ProductResponseModel>) {
                        Logger.i("Product retrieved for ${model.deptCd}")

                        emitter.success(productMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun insertCartItem(cartItem: CartItem): Single<List<CartItem>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        this.insertCartItem(cartItem)

                        emitter.success(this.findCartItems())
                    }
                }
            }
        }

    override fun updateCartItemQuantity(cartItem: CartItem): Single<List<CartItem>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        val existingCartItem = this.findCartItemIsRemovable(cartItem.id, cartItem.prodCd)
                        if (existingCartItem != null) {
                            val newQuantity = cartItem.quantity
                            this.updateCartItemQuantity(newQuantity, existingCartItem.id)
                        }

                        emitter.success(this.findCartItems())
                    }
                }
            }
        }

    override fun removeCartItem(cartItem: CartItem): Single<List<CartItem>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        this.removeCartItemById(cartItem.id)
                        this.removeRemarksByCartItemIdAndProdCode(cartItem.id, cartItem.prodCd)

                        emitter.success(this.findCartItems())
                    }
                }
            }
        }

    override fun getOrderItems(model: GetOrderItemsRequestModel): Single<List<CartItem>> =
        Single.create { emitter ->
            api.getOrderItems(model.tableName, model.tblSysId)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<OrderItemResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<OrderItemResponseModel>) {
                        Logger.i("Order items retrieved")

                        val cartItems = responseData.map {
                            CartItem(
                                prodCd = it.prodCd ?: "",
                                prodShNm = it.prodNm ?: "",
                                price = it.prodPrice ?: "",
                                prodNmCh = it.prodNmCh ?: "",
                                tax1 = "",
                                tax2 = "",
                                imgFilePath = "",
                                quantity = it.quantity ?: 0,
                                isRemovable = false,
                                linkCd = it.linkCd ?: "",
                                remarks = it.remarks ?: ""
                            )
                        }

                        AsyncTask.execute {
                            roomService.runInTransaction {
                                roomService.orderingDao().apply {
                                    this.removeCartItemByIsNotRemovable()
                                    this.insertCartItems(cartItems)

                                    emitter.success(this.findCartItems())
                                }
                            }
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun voidItem(model: VoidItemRequestModel): Single<List<CartItem>> =
        Single.create { emitter ->
            api.voidItem(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("Item voided ${model.linkCd}")

                        AsyncTask.execute {
                            roomService.runInTransaction {
                                roomService.orderingDao().apply {
                                    this.removeCartItemByLinkCd(model.prodCd, model.linkCd)

                                    emitter.success(this.findCartItems())
                                }
                            }
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun getRemarks(model: GetRemarksRequestModel): Single<List<Remark>> =
        Single.create { emitter ->
            api.getRemarks(model.prodCode)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<RemarkResponseModel>>() {
                    override fun onResponseSuccess(responseData: List<RemarkResponseModel>) {
                        Logger.i("Remarks retrieved for ${model.prodCode}")

                        AsyncTask.execute {
                            roomService.runInTransaction {
                                roomService.orderingDao().apply {
                                    val cartItemId = model.cartItemId
                                    val prodCode = model.prodCode
                                    val remarks = responseData.map { data ->
                                        Remark(
                                            cartItemId = cartItemId,
                                            prodCode = prodCode,
                                            kitchenRequestCode = data.kitchenRequestCode ?: "",
                                            kitchenRequestDescription = data.kitchenRequestDescription ?: ""
                                        )
                                    }.toMutableList()
                                    val existingRemarks = this.findRemarksIsSelected(cartItemId, prodCode)

                                    // To mark remark as selected if it exist in local db and is selected
                                    remarks.forEach { remark ->
                                        val selectedRemark = existingRemarks.find { existingRemark ->
                                            remark.kitchenRequestCode == existingRemark.kitchenRequestCode
                                        }

                                        if (selectedRemark != null) {
                                            remark.isSelected = true
                                        }
                                    }

                                    // Add custom remark from existing remarks to new remarks
                                    existingRemarks.find { remark ->
                                        remark.kitchenRequestCode.isBlank()
                                    }?.let { remark ->
                                        remarks.add(remark)
                                    }

                                    // Remove all related remarks and re-insert
                                    this.removeRemarksByCartItemIdAndProdCode(cartItemId, prodCode)
                                    this.insertRemarks(remarks)

                                    emitter.success(remarks)
                                }
                            }
                        }
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        AsyncTask.execute {
                            roomService.runInTransaction {
                                roomService.orderingDao().apply {
                                    val cartItemId = model.cartItemId
                                    val prodCode = model.prodCode
                                    val existingRemarks = this.findRemarksIsSelected(cartItemId, prodCode)
                                    if (existingRemarks.isNotEmpty()) {
                                        emitter.success(existingRemarks)
                                    } else {
                                        emitter.error(throwable)
                                    }
                                }
                            }
                        }
                    }
                })
        }

    override fun submitRemarks(remarks: List<Remark>): Single<List<CartItem>> =
        Single.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        remarks.forEach { remark ->
                            this.removeRemarksByCartItemIdAndProdCode(remark.cartItemId, remark.prodCode)
                        }

                        if (remarks.isNotEmpty()) {
                            this.insertRemarks(remarks)

                            val remarksInString = remarks.filter { it.isSelected }
                                .joinToString(", ") { it.kitchenRequestDescription }
                            val firstRemark = remarks[0]

                            this.updateCartItemRemark(remarksInString, firstRemark.cartItemId)

                            emitter.success(this.findCartItems())
                        }
                    }
                }
            }
        }

    override fun holdBill(model: HoldBillRequestModel): Observable<Void> =
        Observable.create { emitter ->
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.orderingDao().apply {
                        val cartItems = this.findCartItemsIsRemovable()
                        val remarks = this.findRemarks()

                        val items = ArrayList<HoldBillItemRequestModel>()
                        items.addAll(cartItems.map { cartItem ->
                            HoldBillItemRequestModel(
                                productCode = cartItem.prodCd,
                                productName = cartItem.prodShNm,
                                productPrice = cartItem.price,
                                quantity = cartItem.quantity.toString(),
                                multotal = cartItem.getTotal().toString(),
                                // Find remarks of respective CartItem and join with comma
                                remarks = remarks.filter { remark ->
                                    remark.cartItemId == cartItem.id && remark.isSelected
                                }.joinToString(", ") { remark ->
                                    remark.kitchenRequestDescription
                                }
                            )
                        })

                        model.items = items

                        Logger.d(items)

                        api.holdBill(model)
                            .subscribeOn(threadExecutor.getScheduler())
                            .observeOn(postExecutionThread.getScheduler())
                            .subscribe(object : EmptySingleObserverRetrofit() {
                                override fun onResponseSuccess() {
                                    Logger.i("Bill hold")

                                    AsyncTask.execute {
                                        roomService.runInTransaction {
                                            roomService.orderingDao().apply {
                                                // Remove all cart items and remarks once bill is hold
                                                this.removeAllCartItems()
                                                this.removeAllRemarks()

                                                emitter.complete()
                                            }
                                        }
                                    }
                                }

                                override fun onFailure(throwable: Throwable) {
                                    super.onFailure(throwable)
                                    emitter.error(throwable)
                                }
                            })
                    }
                }
            }
        }

    override fun getGuestCheck(model: GetGuestCheckRequestModel): Single<List<String>> =
        Single.create { emitter ->
            api.getGuestCheck(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<List<String>>() {
                    override fun onResponseSuccess(responseData: List<String>) {
                        Logger.i("Guest check retrieved")
                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
