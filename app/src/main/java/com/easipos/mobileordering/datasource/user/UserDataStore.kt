package com.easipos.mobileordering.datasource.user

import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.mapper.user.UserInfoMapper
import com.easipos.mobileordering.models.UserInfo
import io.reactivex.Observable
import io.reactivex.Single

interface UserDataStore {

    fun getUserInfo(mapper: UserInfoMapper): Single<UserInfo>

    fun updateProfile(model: UpdateProfileRequestModel): Observable<Void>
}
