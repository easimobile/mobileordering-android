package com.easipos.mobileordering.datasource.notification

import com.easipos.mobileordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.mobileordering.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NotificationDataSource @Inject constructor(private val api: ApiService,
                                                 private val threadExecutor: ThreadExecutor,
                                                 private val postExecutionThread: PostExecutionThread) : NotificationDataStore {

    override fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable =
        api.registerFcmToken(model.toFormDataBuilder().build())

    override fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable =
        api.removeFcmToken(model.toFormDataBuilder().build())
}
