package com.easipos.mobileordering.datasource.user

import com.easipos.mobileordering.api.misc.EmptySingleObserverRetrofit
import com.easipos.mobileordering.api.misc.SingleObserverRetrofit
import com.easipos.mobileordering.api.requests.BasicRequestModel
import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.api.responses.user.UserInfoResponseModel
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.mapper.user.UserInfoMapper
import com.easipos.mobileordering.models.UserInfo
import com.easipos.mobileordering.use_cases.complete
import com.easipos.mobileordering.use_cases.error
import com.easipos.mobileordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserDataSource @Inject constructor(private val api: ApiService,
                                         private val threadExecutor: ThreadExecutor,
                                         private val postExecutionThread: PostExecutionThread
) : UserDataStore {

    override fun getUserInfo(mapper: UserInfoMapper): Single<UserInfo> =
        Single.create { emitter ->
            api.getUserInfo(BasicRequestModel())
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<UserInfoResponseModel>() {
                    override fun onResponseSuccess(responseData: UserInfoResponseModel) {
                        Logger.i("User info retrieved")

                        emitter.success(mapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun updateProfile(model: UpdateProfileRequestModel): Observable<Void> =
        Observable.create { emitter ->
            api.updateProfile(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : EmptySingleObserverRetrofit() {
                    override fun onResponseSuccess() {
                        Logger.i("User profile updated")

                        emitter.complete()
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
