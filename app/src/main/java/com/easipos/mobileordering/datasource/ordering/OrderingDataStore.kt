package com.easipos.mobileordering.datasource.ordering

import com.easipos.mobileordering.api.requests.ordering.*
import com.easipos.mobileordering.mapper.ordering.MenuMapper
import com.easipos.mobileordering.mapper.ordering.ProductMapper
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Menu
import com.easipos.mobileordering.models.Product
import com.easipos.mobileordering.models.Remark
import io.reactivex.Observable
import io.reactivex.Single

interface OrderingDataStore {

    fun getMenu(menuMapper: MenuMapper): Single<List<Menu>>

    fun getNewProducts(model: GetNewProductsRequestModel, productMapper: ProductMapper): Single<List<Product>>

    fun insertCartItem(cartItem: CartItem): Single<List<CartItem>>

    fun updateCartItemQuantity(cartItem: CartItem): Single<List<CartItem>>

    fun removeCartItem(cartItem: CartItem): Single<List<CartItem>>

    fun getOrderItems(model: GetOrderItemsRequestModel): Single<List<CartItem>>

    fun voidItem(model: VoidItemRequestModel): Single<List<CartItem>>

    fun getRemarks(model: GetRemarksRequestModel): Single<List<Remark>>

    fun submitRemarks(remarks: List<Remark>): Single<List<CartItem>>

    fun holdBill(model: HoldBillRequestModel): Observable<Void>

    fun getGuestCheck(model: GetGuestCheckRequestModel): Single<List<String>>
}
