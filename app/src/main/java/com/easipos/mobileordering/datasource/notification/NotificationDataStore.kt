package com.easipos.mobileordering.datasource.notification

import com.easipos.mobileordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.mobileordering.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationDataStore {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
