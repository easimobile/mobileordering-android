package com.easipos.mobileordering.datasource.auth

import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.mapper.auth.LoginInfoMapper
import com.easipos.mobileordering.models.LoginInfo
import io.reactivex.Completable
import io.reactivex.Single

interface AuthDataStore {

    fun verifyUser(model: VerifyUserRequestModel): Single<Boolean>

    fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Single<LoginInfo>

    fun register(model: RegisterRequestModel): Single<String>

    fun logout(): Completable
}
