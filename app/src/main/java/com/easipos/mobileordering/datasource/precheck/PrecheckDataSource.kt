package com.easipos.mobileordering.datasource.precheck

import com.easipos.mobileordering.api.misc.SingleObserverRetrofit
import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.use_cases.error
import com.easipos.mobileordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrecheckDataSource @Inject constructor(private val api: ApiService,
                                             private val threadExecutor: ThreadExecutor,
                                             private val postExecutionThread: PostExecutionThread) : PrecheckDataStore {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        Single.create { emitter ->
            api.checkVersion(model.toFormDataBuilder().build())
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<Boolean>() {
                    override fun onResponseSuccess(responseData: Boolean) {
                        Logger.i("Version checked: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }
}
