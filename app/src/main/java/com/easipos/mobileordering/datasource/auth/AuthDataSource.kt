package com.easipos.mobileordering.datasource.auth

import com.easipos.mobileordering.api.misc.SingleObserverRetrofit
import com.easipos.mobileordering.api.requests.BasicRequestModel
import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.mobileordering.api.services.ApiService
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.mapper.auth.LoginInfoMapper
import com.easipos.mobileordering.models.LoginInfo
import com.easipos.mobileordering.use_cases.error
import com.easipos.mobileordering.use_cases.success
import com.orhanobut.logger.Logger
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthDataSource @Inject constructor(private val api: ApiService,
                                         private val threadExecutor: ThreadExecutor,
                                         private val postExecutionThread: PostExecutionThread
) :
    AuthDataStore {

    override fun verifyUser(model: VerifyUserRequestModel): Single<Boolean> =
        Single.create { emitter ->
            api.verifyUser(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<Boolean>() {
                    override fun onResponseSuccess(responseData: Boolean) {
                        Logger.i("User verified: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun login(model: LoginRequestModel, loginInfoMapper: LoginInfoMapper): Single<LoginInfo> =
        Single.create { emitter ->
            api.login(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<LoginInfoResponseModel>() {
                    override fun onResponseSuccess(responseData: LoginInfoResponseModel) {
                        Logger.i("User logged in")

                        emitter.success(loginInfoMapper.transform(responseData))
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun register(model: RegisterRequestModel): Single<String> =
        Single.create { emitter ->
            api.register(model)
                .subscribeOn(threadExecutor.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(object : SingleObserverRetrofit<String>() {
                    override fun onResponseSuccess(responseData: String) {
                        Logger.i("User registered: $responseData")

                        emitter.success(responseData)
                    }

                    override fun onFailure(throwable: Throwable) {
                        super.onFailure(throwable)
                        emitter.error(throwable)
                    }
                })
        }

    override fun logout(): Completable =
        api.logout(BasicRequestModel())
}
