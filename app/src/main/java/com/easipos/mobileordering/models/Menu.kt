package com.easipos.mobileordering.models

data class Menu(
    val deptCd: String,
    val deptNm: String,
    val deptCh: String,
    val dispOrder: String
)