package com.easipos.mobileordering.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import io.github.anderscheow.library.kotlinExt.formatAmount

@Entity
data class CartItem(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val prodCd: String,
    val prodShNm: String,
    val price: String,
    val prodNmCh: String,
    val tax1: String,
    val tax2: String,
    val imgFilePath: String,
    var quantity: Int = 1,
    val isRemovable: Boolean = true,
    val linkCd: String = "",
    val remarks: String = ""
) {

    fun formatPrice(): String {
        return (price.toDoubleOrNull() ?: 0.00).formatAmount()
    }

    fun getTotal(): Double {
        return (price.toDoubleOrNull() ?: 0.0) * quantity
    }
}