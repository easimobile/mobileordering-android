package com.easipos.mobileordering.models

data class LoginInfo(
    val token: String,
    val currency: String
)