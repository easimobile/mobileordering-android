package com.easipos.mobileordering.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserInfo(
    val uid: String,
    val profilePictureInBase64: String?,
    val name: String,
    val email: String,
    val phoneNumber: String,
    val currency: String
) : Parcelable