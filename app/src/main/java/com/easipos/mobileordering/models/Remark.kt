package com.easipos.mobileordering.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Remark(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val cartItemId: Int,
    val prodCode: String,
    val kitchenRequestCode: String,
    val kitchenRequestDescription: String,
    var isSelected: Boolean = false
) : Parcelable