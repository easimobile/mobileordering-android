package com.easipos.mobileordering.models

import android.content.Context
import com.easipos.mobileordering.util.formatCurrencyAmount

data class Product(
    val prodCd: String,
    val prodShNm: String,
    val price: String,
    val prodNmCh: String,
    val tax1: String,
    val tax2: String,
    val imgFilePath: String,
    var quantity: Int = 1
) {

    fun formatPrice(context: Context): String {
        return context.formatCurrencyAmount(price)
    }
}