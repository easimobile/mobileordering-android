package com.easipos.mobileordering.models

data class Auth(val token: String)