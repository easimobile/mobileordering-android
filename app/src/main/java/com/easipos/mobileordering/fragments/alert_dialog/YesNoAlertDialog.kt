package com.easipos.mobileordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.mobileordering.R
import com.easipos.mobileordering.base.CustomAlertDialog
import kotlinx.android.synthetic.main.alert_dialog_yes_no.*

class YesNoAlertDialog(context: Context,
                       private val message: CharSequence,
                       private val onNo: () -> Unit,
                       private val onYes: () -> Unit) : CustomAlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_yes_no)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        text_view_message.text = message

        button_no.setOnClickListener {
            onNo()
            dismiss()
        }

        button_yes.setOnClickListener {
            onYes()
            dismiss()
        }
    }
}