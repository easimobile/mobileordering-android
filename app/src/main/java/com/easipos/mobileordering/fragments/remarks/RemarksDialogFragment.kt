package com.easipos.mobileordering.fragments.remarks

import android.app.Dialog
import android.os.Bundle
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.easipos.mobileordering.R
import com.easipos.mobileordering.adapters.RemarkAdapter
import com.easipos.mobileordering.base.CustomBaseDialogFragment
import com.easipos.mobileordering.bundle.ParcelData
import com.easipos.mobileordering.models.Remark
import com.easipos.mobileordering.tools.GridSpacingItemDecoration
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.withContext
import kotlinx.android.synthetic.main.dialog_fragment_remarks.*
import org.jetbrains.anko.displayMetrics

class RemarksDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(cartItemId: Int, prodCode: String, remarks: List<Remark>): RemarksDialogFragment {
            return RemarksDialogFragment().apply {
                arguments = Bundle().apply {
                    this.putInt(ParcelData.CART_ITEM_ID, cartItemId)
                    this.putString(ParcelData.PROD_CODE, prodCode)
                    this.putParcelableArrayList(ParcelData.REMARKS, ArrayList(remarks))
                }
            }
        }
    }

    private val cartItemId by argument<Int>(ParcelData.CART_ITEM_ID)
    private val prodCode by argument<String>(ParcelData.PROD_CODE)
    private val remarks by argument(ParcelData.REMARKS, ArrayList<Remark>())

    private var adapter: RemarkAdapter? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(true)
        }
    }

    override fun onStart() {
        super.onStart()
        activity?.let { activity ->
            dialog?.window?.setLayout((activity.displayMetrics.widthPixels * 0.5).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_remarks

    override fun init() {
        super.init()
        setupViews()
        setupListeners()
        setupRecyclerView()
    }

    private fun setupViews() {
        remarks.find { remark ->
            remark.kitchenRequestCode.isBlank()
        }?.let { remark ->
            edit_text_remark.setText(remark.kitchenRequestDescription)
        }
    }

    private fun setupListeners() {
        button_ok.setOnClickListener {
            submitRemarks()
        }
    }

    private fun setupRecyclerView() {
        withContext { context ->
            adapter = RemarkAdapter(context)
            recycler_view_remark.apply {
                this.layoutManager = GridLayoutManager(context, 2)
                this.adapter = this@RemarksDialogFragment.adapter
                this.addItemDecoration(GridSpacingItemDecoration(2, 16, true))
            }
            adapter?.items = remarks.filter { remark ->
                // Remove custom remark to place in the listing
                remark.kitchenRequestCode != ""
            }.toMutableList()
        }
    }

    private fun submitRemarks() {
        val remarks = adapter?.items ?: ArrayList()
        if (edit_text_remark.text.isNotBlank() && cartItemId != null && prodCode != null) {
            remarks.add(Remark(
                cartItemId = cartItemId!!,
                prodCode = prodCode!!,
                kitchenRequestCode = "",
                kitchenRequestDescription = edit_text_remark.text.toString().trim(),
                isSelected = true
            ))
        }

        Logger.d(remarks)

//        (activity as? MainActivity)?.submitRemarks(remarks)
        dismissAllowingStateLoss()
    }
}