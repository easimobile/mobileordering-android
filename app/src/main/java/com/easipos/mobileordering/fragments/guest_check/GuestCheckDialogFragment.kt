package com.easipos.mobileordering.fragments.guest_check

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.easipos.mobileordering.R
import com.easipos.mobileordering.base.CustomBaseDialogFragment
import com.easipos.mobileordering.bundle.ParcelData
import io.github.anderscheow.library.kotlinExt.argument
import kotlinx.android.synthetic.main.dialog_fragment_guest_check.*

class GuestCheckDialogFragment : CustomBaseDialogFragment() {

    companion object {
        fun newInstance(receipt: List<String?>): GuestCheckDialogFragment {
            return GuestCheckDialogFragment().apply {
                arguments = Bundle().apply {
                    this.putCharSequenceArrayList(ParcelData.RECEIPT, ArrayList(receipt))
                }
            }
        }
    }

    private val receipt by argument(ParcelData.RECEIPT, ArrayList<CharSequence>())

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            this.setCanceledOnTouchOutside(true)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun getResLayout(): Int = R.layout.dialog_fragment_guest_check

    override fun init() {
        super.init()
        setupListeners()
        setupViews()
    }

    private fun setupViews() {
        text_view_receipt.text = receipt.joinToString("\n").replace("null", "")
        text_view_receipt.movementMethod = ScrollingMovementMethod()
    }

    private fun setupListeners() {
        layout_root.setOnClickListener {
            dismiss()
        }
    }
}