package com.easipos.mobileordering.fragments.alert_dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import com.easipos.mobileordering.R
import com.easipos.mobileordering.base.CustomAlertDialog
import kotlinx.android.synthetic.main.alert_dialog_logout.*

class LogoutAlertDialog(context: Context,
                        private val onLogout: () -> Unit) : CustomAlertDialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.alert_dialog_logout)

        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        button_no.setOnClickListener {
            dismiss()
        }

        button_yes.setOnClickListener {
            onLogout()
            dismiss()
        }
    }
}