package com.easipos.mobileordering.di.components

import com.easipos.mobileordering.activities.change_password.ChangePasswordActivity
import com.easipos.mobileordering.activities.edit_profile.EditProfileActivity
import com.easipos.mobileordering.activities.login.LoginActivity
import com.easipos.mobileordering.activities.main.MainActivity
import com.easipos.mobileordering.activities.register.RegisterActivity
import com.easipos.mobileordering.activities.reset_password.ResetPasswordActivity
import com.easipos.mobileordering.activities.splash.SplashActivity
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.di.scope.PerActivity
import dagger.Component

@PerActivity
@Component(dependencies = [(AppComponent::class)], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(mainActivity: MainActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(registerActivity: RegisterActivity)

    fun inject(resetPasswordActivity: ResetPasswordActivity)

    fun inject(editProfileActivity: EditProfileActivity)

    fun inject(changePasswordActivity: ChangePasswordActivity)
}
