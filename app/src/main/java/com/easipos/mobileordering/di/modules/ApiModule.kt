package com.easipos.mobileordering.di.modules

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.api.misc.AuthInterceptor
import com.easipos.mobileordering.api.misc.AuthOkhttpClient
import com.easipos.mobileordering.api.misc.TokenAuthenticator
import com.easipos.mobileordering.api.services.Api
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule(private val easi: Easi,
                private val userAgent: String,
                private val endpoint: String,
                private val authorisation: String) {

    @Provides
    @Singleton
    fun provideAuthInterceptor(): AuthInterceptor {
        return AuthInterceptor(userAgent, authorisation)
    }

    @Provides
    @Singleton
    fun provideTokenAuthenticator(): TokenAuthenticator {
        return TokenAuthenticator(easi)
    }

    @Provides
    @Singleton
    fun provideAuthOkHttpClient(authInterceptor: AuthInterceptor, tokenAuthenticator: TokenAuthenticator): AuthOkhttpClient {
        return AuthOkhttpClient(authInterceptor, tokenAuthenticator)
    }

    @Provides
    @Singleton
    fun provideCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory {
        val gson = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create()
        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    fun provideAuthApi(authOkhttpClient: AuthOkhttpClient,
                       callAdapterFactory: RxJava2CallAdapterFactory,
                       gsonConverterFactory: GsonConverterFactory): Api {
        return Retrofit.Builder()
                .baseUrl(endpoint)
                .client(authOkhttpClient.getAuthOkhttpClient())
                .addCallAdapterFactory(callAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .build()
                .create(Api::class.java)
    }
}
