package com.easipos.mobileordering.di.modules

import androidx.fragment.app.Fragment
import dagger.Module

@Module
class FragmentModule(private val fragment: Fragment)
