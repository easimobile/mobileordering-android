package com.easipos.mobileordering.di.modules

import android.app.Activity
import com.easipos.mobileordering.activities.change_password.navigation.ChangePasswordNavigation
import com.easipos.mobileordering.activities.change_password.navigation.ChangePasswordNavigationImpl
import com.easipos.mobileordering.activities.edit_profile.navigation.EditProfileNavigation
import com.easipos.mobileordering.activities.edit_profile.navigation.EditProfileNavigationImpl
import com.easipos.mobileordering.activities.login.navigation.LoginNavigation
import com.easipos.mobileordering.activities.login.navigation.LoginNavigationImpl
import com.easipos.mobileordering.activities.main.navigation.MainNavigation
import com.easipos.mobileordering.activities.main.navigation.MainNavigationImpl
import com.easipos.mobileordering.activities.register.navigation.RegisterNavigation
import com.easipos.mobileordering.activities.register.navigation.RegisterNavigationImpl
import com.easipos.mobileordering.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.mobileordering.activities.reset_password.navigation.ResetPasswordNavigationImpl
import com.easipos.mobileordering.activities.splash.navigation.SplashNavigation
import com.easipos.mobileordering.activities.splash.navigation.SplashNavigationImpl
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    fun provideSplashNavigation(): SplashNavigation {
        return SplashNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideLoginNavigation(): LoginNavigation {
        return LoginNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideRegisterNavigation(): RegisterNavigation {
        return RegisterNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideResetPasswordNavigation(): ResetPasswordNavigation {
        return ResetPasswordNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideEditProfileNavigation(): EditProfileNavigation {
        return EditProfileNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideChangePasswordNavigation(): ChangePasswordNavigation {
        return ChangePasswordNavigationImpl(activity.window.decorView.rootView)
    }

    @Provides
    fun provideMainNavigation(): MainNavigation {
        return MainNavigationImpl(activity.window.decorView.rootView)
    }
}
