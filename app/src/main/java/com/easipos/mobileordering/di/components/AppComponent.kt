package com.easipos.mobileordering.di.components

import android.app.Application
import android.content.Context
import com.easipos.mobileordering.api.services.Api
import com.easipos.mobileordering.di.modules.ApiModule
import com.easipos.mobileordering.di.modules.AppModule
import com.easipos.mobileordering.di.modules.DatabaseModule
import com.easipos.mobileordering.executor.DiskIOExecutor
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.managers.FcmManager
import com.easipos.mobileordering.managers.PushNotificationManager
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.repositories.notification.NotificationRepository
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.repositories.precheck.PrecheckRepository
import com.easipos.mobileordering.repositories.user.UserRepository
import com.easipos.mobileordering.room.RoomService
import dagger.Component
import io.github.anderscheow.validator.Validator
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, DatabaseModule::class])
interface AppComponent {

    fun inject(app: Application)

    val app: Application

    fun context(): Context

    fun validator(): Validator

    fun roomService(): RoomService

    fun pushNotificationManager(): PushNotificationManager

    fun fcmManager(): FcmManager

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun diskIoExecutor(): DiskIOExecutor

    fun api(): Api

    fun precheckRepository(): PrecheckRepository

    fun authRepository(): AuthRepository

    fun userRepository(): UserRepository

    fun orderingRepository(): OrderingRepository

    fun notificationRepository(): NotificationRepository
}
