package com.easipos.mobileordering.di.components

import com.easipos.mobileordering.di.modules.FragmentModule
import com.easipos.mobileordering.di.scope.PerFragment
import dagger.Component

@PerFragment
@Component(dependencies = [(AppComponent::class)], modules = [FragmentModule::class])
interface FragmentComponent
