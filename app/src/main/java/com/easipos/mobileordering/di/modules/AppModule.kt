package com.easipos.mobileordering.di.modules

import android.app.Application
import android.content.Context
import com.easipos.mobileordering.executor.*
import com.easipos.mobileordering.managers.FcmManager
import com.easipos.mobileordering.managers.PushNotificationManager
import com.easipos.mobileordering.repositories.auth.AuthDataRepository
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.repositories.notification.NotificationDataRepository
import com.easipos.mobileordering.repositories.notification.NotificationRepository
import com.easipos.mobileordering.repositories.ordering.OrderingDataRepository
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.repositories.precheck.PrecheckDataRepository
import com.easipos.mobileordering.repositories.precheck.PrecheckRepository
import com.easipos.mobileordering.repositories.user.UserDataRepository
import com.easipos.mobileordering.repositories.user.UserRepository
import com.easipos.mobileordering.services.FcmService
import com.easipos.mobileordering.services.PushNotificationService
import dagger.Module
import dagger.Provides
import io.github.anderscheow.validator.Validator
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun application(): Application {
        return app
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideValidator(context: Context): Validator {
        return Validator.with(context)
    }

    @Provides
    @Singleton
    fun providePushNotificationManager(pushNotificationService: PushNotificationService): PushNotificationManager {
        return PushNotificationManager(pushNotificationService)
    }

    @Provides
    @Singleton
    fun provideFcmManager(fcmService: FcmService): FcmManager {
        return FcmManager(fcmService)
    }

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread {
        return uiThread
    }

    @Provides
    @Singleton
    fun provideDiskIOExecutor(IOExecutor: IOExecutor): DiskIOExecutor {
        return IOExecutor
    }

    @Provides
    @Singleton
    fun providePrecheckRepository(precheckDataRepository: PrecheckDataRepository): PrecheckRepository {
        return precheckDataRepository
    }

    @Provides
    @Singleton
    fun provideAuthRepository(authDataRepository: AuthDataRepository): AuthRepository {
        return authDataRepository
    }

    @Provides
    @Singleton
    fun provideUserRepository(userDataRepository: UserDataRepository): UserRepository {
        return userDataRepository
    }

    @Provides
    @Singleton
    fun provideOrderingRepository(orderingDataRepository: OrderingDataRepository): OrderingRepository {
        return orderingDataRepository
    }

    @Provides
    @Singleton
    fun provideNotificationRepository(notificationDataRepository: NotificationDataRepository): NotificationRepository {
        return notificationDataRepository
    }
}
