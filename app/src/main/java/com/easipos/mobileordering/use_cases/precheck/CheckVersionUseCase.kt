package com.easipos.mobileordering.use_cases.precheck

import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.precheck.PrecheckRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class CheckVersionUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                              postExecutionThread: PostExecutionThread,
                                              private val repository: PrecheckRepository)
    : AbsRxSingleUseCase<Boolean, CheckVersionUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<Boolean> =
            repository.checkVersion(params.model)

    class Params private constructor(val model: CheckVersionRequestModel) {
        companion object {
            fun createQuery(model: CheckVersionRequestModel): Params {
                return Params(model)
            }
        }
    }
}
