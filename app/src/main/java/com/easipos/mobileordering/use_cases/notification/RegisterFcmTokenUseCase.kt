package com.easipos.mobileordering.use_cases.notification

import com.easipos.mobileordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.notification.NotificationRepository
import com.easipos.mobileordering.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class RegisterFcmTokenUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                  postExecutionThread: PostExecutionThread,
                                                  private val repository: NotificationRepository)
    : AbsRxCompletableUseCase<RegisterFcmTokenUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createCompletable(params: Params): Completable =
            repository.registerFcmToken(params.model)

    class Params private constructor(val model: RegisterFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RegisterFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
