package com.easipos.mobileordering.use_cases.auth

import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class LogoutUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                        postExecutionThread: PostExecutionThread,
                                        private val repository: AuthRepository
)
    : AbsRxCompletableUseCase<LogoutUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createCompletable(params: Params): Completable =
            repository.logout()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
