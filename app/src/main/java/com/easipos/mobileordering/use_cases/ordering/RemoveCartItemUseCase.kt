package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class RemoveCartItemUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                postExecutionThread: PostExecutionThread,
                                                private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<CartItem>, RemoveCartItemUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<CartItem>> =
            repository.removeCartItem(params.cartItem)

    class Params private constructor(val cartItem: CartItem) {
        companion object {
            fun createQuery(cartItem: CartItem): Params {
                return Params(cartItem)
            }
        }
    }
}
