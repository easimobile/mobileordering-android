package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.api.requests.ordering.GetOrderItemsRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetOrderItemsUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<CartItem>, GetOrderItemsUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<CartItem>> =
            repository.getOrderItems(params.model)

    class Params private constructor(val model: GetOrderItemsRequestModel) {
        companion object {
            fun createQuery(model: GetOrderItemsRequestModel): Params {
                return Params(model)
            }
        }
    }
}
