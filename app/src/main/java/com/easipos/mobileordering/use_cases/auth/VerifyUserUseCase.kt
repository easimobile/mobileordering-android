package com.easipos.mobileordering.use_cases.auth

import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class VerifyUserUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread,
                                            private val repository: AuthRepository
)
    : AbsRxSingleUseCase<Boolean, VerifyUserUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<Boolean> =
            repository.verifyUser(params.model)

    class Params private constructor(val model: VerifyUserRequestModel) {
        companion object {
            fun createQuery(model: VerifyUserRequestModel): Params {
                return Params(model)
            }
        }
    }
}
