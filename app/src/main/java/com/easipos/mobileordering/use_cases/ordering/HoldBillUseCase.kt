package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.api.requests.ordering.HoldBillRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import javax.inject.Inject

class HoldBillUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                          postExecutionThread: PostExecutionThread,
                                          private val repository: OrderingRepository
)
    : AbsRxObservableUseCase<Void, HoldBillUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Params): Observable<Void> =
            repository.holdBill(params.model)

    class Params private constructor(val model: HoldBillRequestModel) {
        companion object {
            fun createQuery(model: HoldBillRequestModel): Params {
                return Params(model)
            }
        }
    }
}
