package com.easipos.mobileordering.use_cases.user

import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.UserInfo
import com.easipos.mobileordering.repositories.user.UserRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetUserInfoUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                             postExecutionThread: PostExecutionThread,
                                             private val repository: UserRepository
)
    : AbsRxSingleUseCase<UserInfo, GetUserInfoUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<UserInfo> =
            repository.getUserInfo()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
