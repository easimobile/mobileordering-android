package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.api.requests.ordering.GetRemarksRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.Remark
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetRemarksUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                            postExecutionThread: PostExecutionThread,
                                            private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<Remark>, GetRemarksUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<Remark>> =
            repository.getRemarks(params.model)

    class Params private constructor(val model: GetRemarksRequestModel) {
        companion object {
            fun createQuery(model: GetRemarksRequestModel): Params {
                return Params(model)
            }
        }
    }
}
