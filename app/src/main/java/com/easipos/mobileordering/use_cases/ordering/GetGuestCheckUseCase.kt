package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.api.requests.ordering.GetGuestCheckRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetGuestCheckUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<String>, GetGuestCheckUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<String>> =
            repository.getGuestCheck(params.model)

    class Params private constructor(val model: GetGuestCheckRequestModel) {
        companion object {
            fun createQuery(model: GetGuestCheckRequestModel): Params {
                return Params(model)
            }
        }
    }
}
