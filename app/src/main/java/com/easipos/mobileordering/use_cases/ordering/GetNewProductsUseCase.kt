package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.api.requests.ordering.GetNewProductsRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.Product
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetNewProductsUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                postExecutionThread: PostExecutionThread,
                                                private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<Product>, GetNewProductsUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<Product>> =
            repository.getNewProducts(params.model)

    class Params private constructor(val model: GetNewProductsRequestModel) {
        companion object {
            fun createQuery(model: GetNewProductsRequestModel): Params {
                return Params(model)
            }
        }
    }
}
