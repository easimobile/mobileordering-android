package com.easipos.mobileordering.use_cases.user

import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.user.UserRepository
import com.easipos.mobileordering.use_cases.base.AbsRxObservableUseCase
import io.reactivex.Observable
import javax.inject.Inject

class UpdateProfileUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               private val repository: UserRepository
)
    : AbsRxObservableUseCase<Void, UpdateProfileUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createObservable(params: Params): Observable<Void> =
            repository.updateProfile(params.model)

    class Params private constructor(val model: UpdateProfileRequestModel) {
        companion object {
            fun createQuery(model: UpdateProfileRequestModel): Params {
                return Params(model)
            }
        }
    }
}
