package com.easipos.mobileordering.use_cases.auth

import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class RegisterUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                          postExecutionThread: PostExecutionThread,
                                          private val repository: AuthRepository
)
    : AbsRxSingleUseCase<String, RegisterUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<String> =
            repository.register(params.model)

    class Params private constructor(val model: RegisterRequestModel) {
        companion object {
            fun createQuery(model: RegisterRequestModel): Params {
                return Params(model)
            }
        }
    }
}
