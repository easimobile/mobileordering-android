package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.Menu
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class GetMenuUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                         postExecutionThread: PostExecutionThread,
                                         private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<Menu>, GetMenuUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<Menu>> =
            repository.getMenu()

    class Params private constructor() {
        companion object {
            fun createQuery(): Params {
                return Params()
            }
        }
    }
}
