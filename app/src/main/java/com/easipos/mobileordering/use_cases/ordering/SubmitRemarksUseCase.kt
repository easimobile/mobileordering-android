package com.easipos.mobileordering.use_cases.ordering

import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Remark
import com.easipos.mobileordering.repositories.ordering.OrderingRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class SubmitRemarksUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                               postExecutionThread: PostExecutionThread,
                                               private val repository: OrderingRepository
)
    : AbsRxSingleUseCase<List<CartItem>, SubmitRemarksUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<List<CartItem>> =
            repository.submitRemarks(params.remarks)

    class Params private constructor(val remarks: List<Remark>) {
        companion object {
            fun createQuery(remarks: List<Remark>): Params {
                return Params(remarks)
            }
        }
    }
}
