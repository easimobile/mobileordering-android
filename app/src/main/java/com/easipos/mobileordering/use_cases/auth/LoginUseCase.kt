package com.easipos.mobileordering.use_cases.auth

import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.models.LoginInfo
import com.easipos.mobileordering.repositories.auth.AuthRepository
import com.easipos.mobileordering.use_cases.base.AbsRxSingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class LoginUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                       postExecutionThread: PostExecutionThread,
                                       private val repository: AuthRepository
)
    : AbsRxSingleUseCase<LoginInfo, LoginUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createSingle(params: Params): Single<LoginInfo> =
            repository.login(params.model)

    class Params private constructor(val model: LoginRequestModel) {
        companion object {
            fun createQuery(model: LoginRequestModel): Params {
                return Params(model)
            }
        }
    }
}
