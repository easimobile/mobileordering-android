package com.easipos.mobileordering.use_cases.notification

import com.easipos.mobileordering.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.mobileordering.executor.PostExecutionThread
import com.easipos.mobileordering.executor.ThreadExecutor
import com.easipos.mobileordering.repositories.notification.NotificationRepository
import com.easipos.mobileordering.use_cases.base.AbsRxCompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class RemoveFcmTokenUseCase @Inject constructor(threadExecutor: ThreadExecutor,
                                                postExecutionThread: PostExecutionThread,
                                                private val repository: NotificationRepository)
    : AbsRxCompletableUseCase<RemoveFcmTokenUseCase.Params>(threadExecutor, postExecutionThread) {

    override fun createCompletable(params: Params): Completable =
            repository.removeFcmToken(params.model)

    class Params private constructor(val model: RemoveFcmTokenRequestModel) {
        companion object {
            fun createQuery(model: RemoveFcmTokenRequestModel): Params {
                return Params(model)
            }
        }
    }
}
