package com.easipos.mobileordering.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.util.Base64
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.managers.PushNotificationManager
import com.easipos.mobileordering.managers.PushNotificationManager.Companion.FCM_BODY
import com.easipos.mobileordering.managers.PushNotificationManager.Companion.FCM_TITLE
import com.easipos.mobileordering.tools.Preference
import com.tapadoo.alerter.Alerter
import io.github.anderscheow.library.kotlinExt.findColor
import org.json.JSONObject
import java.math.BigInteger
import java.security.MessageDigest

val languages = mapOf("zh" to "中文", "en" to "English")

fun defaultRequestOption(): RequestOptions {
    return RequestOptions()
}

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(this.context)
        .load(imageUrl)
        .apply(defaultRequestOption())
//            .transition(DrawableTransitionOptions().crossFade(500))
        .into(this)
}

fun ImageView.loadImageBase64(imgBase64: String) {
    try {
        val imageBytes = Base64.decode(imgBase64, Base64.DEFAULT)
        Glide.with(this.context)
            .load(imageBytes)
            .apply(defaultRequestOption())
//            .transition(DrawableTransitionOptions().crossFade(500))
            .into(this)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}


fun changeLanguage(activity: Activity, language: String) {
    if (Preference.prefLanguageCode != language) {
        Preference.prefLanguageCode = language
        Easi.localeManager.setNewLocale(activity, language)
        activity.recreate()
    }
}

fun showAlerter(easi: Easi, pushNotificationManager: PushNotificationManager, jsonObject: JSONObject) {
    jsonObject.takeIf {
        jsonObject.has(FCM_TITLE) && jsonObject.has(FCM_BODY)
    }.run {
        this?.let { data ->
            easi.currentActivity?.let { activity ->
                Alerter.create(activity)
                    .setTitle(data.getString(FCM_TITLE) ?: "")
                    .setText(data.getString(FCM_BODY) ?: "")
                    .setBackgroundColorInt(activity.findColor(R.color.colorPrimary))
                    .setDuration(5000)
                    .setIconColorFilter(Color.WHITE)
                    .enableSwipeToDismiss()
                    .enableVibration(true)
                    .setOnClickListener(View.OnClickListener {
                        Alerter.hide()

                        pushNotificationManager.openNotification(easi, jsonObject, true)
                    })
                    //.setOnShowListener { Tracker.trackScreen(this@CustomLifecycleActivity, Screen.IN_APP_NOTIFICATION) }
                    .show()
            }
        }
    }
}

fun Context.formatCurrencyAmount(amount: String): String {
    return this.getString(R.string.label_amount, Preference.prefCurrency, amount)
}