package com.easipos.mobileordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.mobileordering.R
import com.easipos.mobileordering.databinding.ViewCartItemBinding
import com.easipos.mobileordering.models.CartItem
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_cart_item.*

class CartItemAdapter(context: Context,
                      private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<CartItem>(context) {

    interface OnGestureDetectedListener {
        fun onRequestRemarks(cartItem: CartItem)
        fun onUpdateQuantity(cartItem: CartItem)
        fun onRemoveItem(cartItem: CartItem)
        fun onVoidItem(cartItem: CartItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewCartItemBinding>(
            layoutInflater, R.layout.view_cart_item, parent, false)
        return CartItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class CartItemViewHolder(private val binding: ViewCartItemBinding)
        : BaseViewHolder<CartItem>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: CartItem) {
            if (item.isRemovable) {
                button_remark.setOnClickListener {
                    listener.onRequestRemarks(item)
                }

                button_minus.setOnClickListener {
                    minusQuantity(item)
                }

                button_plus.setOnClickListener {
                    addQuantity(item)
                }

                image_view_remove.setOnClickListener {
                    listener.onRemoveItem(item)
                }

                text_view_void.setOnClickListener(null)

                button_remark.setBackgroundResource(R.drawable.bg_button_selected)
                button_minus.setBackgroundResource(R.drawable.bg_button_selected)
                button_plus.setBackgroundResource(R.drawable.bg_button_selected)

                image_view_remove.visible()
                text_view_void.gone()
            } else {
                button_remark.setOnClickListener(null)
                button_minus.setOnClickListener(null)
                button_plus.setOnClickListener(null)
                image_view_remove.setOnClickListener(null)

                text_view_void.setOnClickListener {
                    listener.onVoidItem(item)
                }

                button_remark.setBackgroundResource(R.drawable.bg_button_disable)
                button_minus.setBackgroundResource(R.drawable.bg_button_disable)
                button_plus.setBackgroundResource(R.drawable.bg_button_disable)

                image_view_remove.gone()
                text_view_void.visible()
            }
        }

        override fun onClick(view: View, item: CartItem?) {
        }

        private fun minusQuantity(item: CartItem) {
            if (item.quantity > 1) {
                item.quantity -= 1
                listener.onUpdateQuantity(item)
                notifyDataSetChanged()
            }
        }

        private fun addQuantity(item: CartItem) {
            if (item.quantity < 99) {
                item.quantity += 1
                listener.onUpdateQuantity(item)
                notifyDataSetChanged()
            }
        }
    }
}