package com.easipos.mobileordering.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.mobileordering.R
import com.easipos.mobileordering.databinding.ViewProductBinding
import com.easipos.mobileordering.models.Product
import com.easipos.mobileordering.util.loadImage
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product.*

class ProductAdapter(context: Context,
                     private val listener: OnGestureDetectedListener) : BaseRecyclerViewAdapter<Product>(context) {

    interface OnGestureDetectedListener {
        fun onAddProduct(view: AppCompatImageView, product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewProductBinding>(
            layoutInflater, R.layout.view_product, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ProductViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class ProductViewHolder(private val binding: ViewProductBinding)
        : BaseViewHolder<Product>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Product) {
            if (item.imgFilePath.isNotBlank()) {
                image_view_product.loadImage(item.imgFilePath)
            }

            button_minus.setOnClickListener {
                minusQuantity(item)
            }

            button_plus.setOnClickListener {
                addQuantity(item)
            }

            button_add.setOnClickListener {
                listener.onAddProduct(image_view_product, item)
                item.quantity = 1
                notifyDataSetChanged()
            }
        }

        override fun onClick(view: View, item: Product?) {
        }

        private fun minusQuantity(item: Product) {
            if (item.quantity > 1) {
                item.quantity -= 1
                notifyDataSetChanged()
            }
        }

        private fun addQuantity(item: Product) {
            if (item.quantity < 99) {
                item.quantity += 1
                notifyDataSetChanged()
            }
        }
    }
}