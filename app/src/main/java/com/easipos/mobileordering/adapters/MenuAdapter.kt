package com.easipos.mobileordering.adapters

import android.content.Context
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.RecyclerView
import com.easipos.mobileordering.R
import com.easipos.mobileordering.databinding.ViewMenuBinding
import com.easipos.mobileordering.models.Menu
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer

class MenuAdapter(context: Context) : BaseRecyclerViewAdapter<Menu>(context) {

    init {
        setHasStableIds(true)
    }

    var tracker: SelectionTracker<Long>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewMenuBinding>(
            layoutInflater, R.layout.view_menu, parent, false)
        return MenuViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MenuViewHolder) {
            holder.bind(items[holder.adapterPosition])
            tracker?.let {
                holder.setActivated(it.isSelected(position.toLong()))
            }
        }
    }

    override fun getItemId(position: Int): Long = position.toLong()

    inner class MenuViewHolder(private val binding: ViewMenuBinding)
        : BaseViewHolder<Menu>(binding), LayoutContainer {

        override val containerView: View?
            get() = binding.root

        override fun extraBinding(item: Menu) {

        }

        override fun onClick(view: View, item: Menu?) {
        }

        fun setActivated(isActivated: Boolean = false) {
            itemView.isActivated = isActivated
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = adapterPosition
                override fun getSelectionKey(): Long? = itemId
                override fun inSelectionHotspot(e: MotionEvent): Boolean = true
            }
    }
}

class MenuItemKeyProvider(private val recyclerView: RecyclerView) :
    ItemKeyProvider<Long>(SCOPE_MAPPED) {

    override fun getKey(position: Int): Long? {
        return recyclerView.adapter?.getItemId(position)
    }

    override fun getPosition(key: Long): Int {
        val viewHolder = recyclerView.findViewHolderForItemId(key)
        return viewHolder?.layoutPosition ?: RecyclerView.NO_POSITION
    }
}

class MenuItemDetailsLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Long>() {
    override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? {
        val view = recyclerView.findChildViewUnder(event.x, event.y)
        if (view != null) {
            return (recyclerView.getChildViewHolder(view) as MenuAdapter.MenuViewHolder)
                .getItemDetails()
        }
        return null
    }
}