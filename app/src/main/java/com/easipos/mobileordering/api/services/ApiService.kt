package com.easipos.mobileordering.api.services

import com.easipos.mobileordering.api.misc.EmptyResponseModel
import com.easipos.mobileordering.api.misc.ResponseModel
import com.easipos.mobileordering.api.requests.BasicRequestModel
import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.api.requests.ordering.GetGuestCheckRequestModel
import com.easipos.mobileordering.api.requests.ordering.HoldBillRequestModel
import com.easipos.mobileordering.api.requests.ordering.VoidItemRequestModel
import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.mobileordering.api.responses.ordering.MenuResponseModel
import com.easipos.mobileordering.api.responses.ordering.OrderItemResponseModel
import com.easipos.mobileordering.api.responses.ordering.ProductResponseModel
import com.easipos.mobileordering.api.responses.ordering.RemarkResponseModel
import com.easipos.mobileordering.api.responses.user.UserInfoResponseModel
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiService @Inject constructor(private val api: Api) : Api {

    override fun checkVersion(body: RequestBody): Single<ResponseModel<Boolean>> {
        return api.checkVersion(body)
    }

    override fun verifyUser(body: VerifyUserRequestModel): Single<ResponseModel<Boolean>> {
        return api.verifyUser(body)
    }

    override fun login(body: LoginRequestModel): Single<ResponseModel<LoginInfoResponseModel>> {
        return api.login(body)
    }

    override fun register(body: RegisterRequestModel): Single<ResponseModel<String>> {
        return api.register(body)
    }

    override fun logout(body: BasicRequestModel): Completable {
        return api.logout(body)
    }

    override fun getUserInfo(body: BasicRequestModel): Single<ResponseModel<UserInfoResponseModel>> {
        return api.getUserInfo(body)
    }

    override fun updateProfile(body: UpdateProfileRequestModel): Single<EmptyResponseModel> {
        return api.updateProfile(body)
    }

    override fun getMenu(): Single<ResponseModel<List<MenuResponseModel>>> {
        return api.getMenu()
    }

    override fun getNewProducts(menu: String): Single<ResponseModel<List<ProductResponseModel>>> {
        return api.getNewProducts(menu)
    }

    override fun getOrderItems(
        tableName: String,
        tblSysId: String
    ): Single<ResponseModel<List<OrderItemResponseModel>>> {
        return api.getOrderItems(tableName, tblSysId)
    }

    override fun voidItem(body: VoidItemRequestModel): Single<EmptyResponseModel> {
        return api.voidItem(body)
    }

    override fun getRemarks(prodCode: String): Single<ResponseModel<List<RemarkResponseModel>>> {
        return api.getRemarks(prodCode)
    }

    override fun holdBill(body: HoldBillRequestModel): Single<EmptyResponseModel> {
        return api.holdBill(body)
    }

    override fun getGuestCheck(body: GetGuestCheckRequestModel): Single<ResponseModel<List<String>>> {
        return api.getGuestCheck(body)
    }

    override fun registerFcmToken(body: RequestBody): Completable {
        return api.registerFcmToken(body)
    }

    override fun removeFcmToken(body: RequestBody): Completable {
        return api.removeFcmToken(body)
    }
}
