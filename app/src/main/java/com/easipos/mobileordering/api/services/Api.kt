package com.easipos.mobileordering.api.services

import com.easipos.mobileordering.api.ApiEndpoint
import com.easipos.mobileordering.api.misc.EmptyResponseModel
import com.easipos.mobileordering.api.misc.ResponseModel
import com.easipos.mobileordering.api.requests.BasicRequestModel
import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.api.requests.ordering.GetGuestCheckRequestModel
import com.easipos.mobileordering.api.requests.ordering.HoldBillRequestModel
import com.easipos.mobileordering.api.requests.ordering.VoidItemRequestModel
import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.mobileordering.api.responses.ordering.MenuResponseModel
import com.easipos.mobileordering.api.responses.ordering.OrderItemResponseModel
import com.easipos.mobileordering.api.responses.ordering.ProductResponseModel
import com.easipos.mobileordering.api.responses.ordering.RemarkResponseModel
import com.easipos.mobileordering.api.responses.user.UserInfoResponseModel
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    fun checkVersion(@Body body: RequestBody): Single<ResponseModel<Boolean>>

    @POST(ApiEndpoint.VERIFY_USER)
    fun verifyUser(@Body body: VerifyUserRequestModel): Single<ResponseModel<Boolean>>

    @POST(ApiEndpoint.LOGIN)
    fun login(@Body body: LoginRequestModel): Single<ResponseModel<LoginInfoResponseModel>>

    @POST(ApiEndpoint.REGISTER)
    fun register(@Body body: RegisterRequestModel): Single<ResponseModel<String>>

    @POST(ApiEndpoint.LOGOUT)
    fun logout(@Body body: BasicRequestModel): Completable

    @POST(ApiEndpoint.GET_USER_INFO)
    fun getUserInfo(@Body body: BasicRequestModel): Single<ResponseModel<UserInfoResponseModel>>

    @POST(ApiEndpoint.UPDATE_USER_PROFILE)
    fun updateProfile(@Body body: UpdateProfileRequestModel): Single<EmptyResponseModel>

    @GET(ApiEndpoint.GET_MENU)
    fun getMenu(): Single<ResponseModel<List<MenuResponseModel>>>

    @POST(ApiEndpoint.GET_NEW_PRODUCTS)
    fun getNewProducts(@Query("menu") menu: String): Single<ResponseModel<List<ProductResponseModel>>>

    @GET(ApiEndpoint.GET_ORDER_ITEMS)
    fun getOrderItems(@Query("tableName") tableName: String, @Query("tblSysId") tblSysId: String): Single<ResponseModel<List<OrderItemResponseModel>>>

    @POST(ApiEndpoint.VOID_ITEM)
    fun voidItem(@Body body: VoidItemRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.GET_REMARKS)
    fun getRemarks(@Query("prod_code") prodCode: String): Single<ResponseModel<List<RemarkResponseModel>>>

    @POST(ApiEndpoint.HOLD_BILL)
    fun holdBill(@Body body: HoldBillRequestModel): Single<EmptyResponseModel>

    @POST(ApiEndpoint.GET_GUEST_CHECK)
    fun getGuestCheck(@Body body: GetGuestCheckRequestModel): Single<ResponseModel<List<String>>>

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun registerFcmToken(@Body body: RequestBody): Completable

    @POST(ApiEndpoint.REGISTER_REMOVE_JPUSH_REG_ID)
    fun removeFcmToken(@Body body: RequestBody): Completable
}
