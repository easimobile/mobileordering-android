package com.easipos.mobileordering.api.requests.ordering

data class GetRemarksRequestModel(
    val cartItemId: Int,
    val prodCode: String
)