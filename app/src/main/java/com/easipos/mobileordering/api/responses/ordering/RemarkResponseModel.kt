package com.easipos.mobileordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class RemarkResponseModel(
    @SerializedName("KitchenRequestCode")
    val kitchenRequestCode: String?,

    @SerializedName("KitchenRequestDescription")
    val kitchenRequestDescription: String?
)