package com.easipos.mobileordering.api.requests.user

import com.easipos.mobileordering.managers.UserManager

data class UpdateProfileRequestModel(
    val apiKey: String? = UserManager.token?.token,
    val profilePictureInBase64: String? = null,
    val name: String
)