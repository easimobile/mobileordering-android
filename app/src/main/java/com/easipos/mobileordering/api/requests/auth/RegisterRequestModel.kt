package com.easipos.mobileordering.api.requests.auth

data class RegisterRequestModel(
    var idToken: String? = null,
    var uid: String? = null,
    val name: String,
    val email: String
)