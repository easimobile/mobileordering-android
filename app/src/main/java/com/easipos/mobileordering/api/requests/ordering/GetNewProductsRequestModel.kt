package com.easipos.mobileordering.api.requests.ordering

import com.google.gson.annotations.SerializedName

data class GetNewProductsRequestModel(
    @SerializedName("dept_cd")
    val deptCd: String
)