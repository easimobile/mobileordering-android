package com.easipos.mobileordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class OrderItemResponseModel(
    @SerializedName("PROD_CD")
    val prodCd: String?,

    @SerializedName("PROD_NM")
    val prodNm: String?,

    @SerializedName("quantity")
    val quantity: Int?,

    @SerializedName("remarks")
    val remarks: String?,

    @SerializedName("prod_price")
    val prodPrice: String?,

    @SerializedName("multotal")
    val multotal: String?,

    @SerializedName("link_cd")
    val linkCd: String?,

    @SerializedName("PROD_NM_CH")
    val prodNmCh: String?
)