package com.easipos.mobileordering.api.requests.precheck

import com.easipos.mobileordering.BuildConfig
import com.easipos.mobileordering.api.requests.RequestModel
import okhttp3.MultipartBody

class CheckVersionRequestModel: RequestModel() {

    override fun toFormDataBuilder(): MultipartBody.Builder {
        return super.toFormDataBuilder()
            .addFormDataPart("version", BuildConfig.VERSION_CODE.toString())
            .addFormDataPart("app", "ddcard")
            .addFormDataPart("os", "android")
    }
}