package com.easipos.mobileordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class MenuResponseModel(
    @SerializedName("dept_cd")
    val deptCd: String?,

    @SerializedName("dept_nm")
    val deptNm: String?,

    @SerializedName("dept_ch")
    val deptCh: String?,

    @SerializedName("disp_order")
    val dispOrder: String?
)