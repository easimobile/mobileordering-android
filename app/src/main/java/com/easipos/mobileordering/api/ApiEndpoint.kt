package com.easipos.mobileordering.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val VERIFY_USER = "User/verifyUser"
    const val LOGIN = "User/login"
    const val REGISTER = "User/register"
    const val LOGOUT = "User/logout"

    const val GET_USER_INFO = "User/getUserInformation"
    const val UPDATE_USER_PROFILE = "User/updateUserProfile"

    const val GET_MENU = "api/v1/getMenu"
    const val GET_NEW_PRODUCTS = "api/v1/getNewProducts"
    const val GET_ORDER_ITEMS = "api/v1/getOrderItem"
    const val VOID_ITEM = "api/v1/voidItem"
    const val GET_REMARKS = "api/v1/getSelection"
    const val HOLD_BILL = "api/v1/holdBill"
    const val GET_GUEST_CHECK = "api/v1/getGuestCheck"

    const val REGISTER_REMOVE_JPUSH_REG_ID = "ddcard/apptoken"
}
