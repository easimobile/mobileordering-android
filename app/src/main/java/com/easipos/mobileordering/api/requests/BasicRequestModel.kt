package com.easipos.mobileordering.api.requests

import com.easipos.mobileordering.managers.UserManager

data class BasicRequestModel(
    val apiKey: String? = UserManager.token?.token
)