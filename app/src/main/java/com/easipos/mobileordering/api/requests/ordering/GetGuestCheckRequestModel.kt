package com.easipos.mobileordering.api.requests.ordering

import com.easipos.mobileordering.tools.Preference

data class GetGuestCheckRequestModel(
    val tableName: String = Preference.prefTableName,
    val tblSysId: String = Preference.prefTableSysId
)