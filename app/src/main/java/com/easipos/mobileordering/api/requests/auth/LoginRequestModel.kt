package com.easipos.mobileordering.api.requests.auth

data class LoginRequestModel(
    val idToken: String,
    val uid: String
)