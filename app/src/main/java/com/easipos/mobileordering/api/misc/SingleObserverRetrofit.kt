package com.easipos.mobileordering.api.misc

import com.google.gson.Gson
import com.easipos.mobileordering.util.ApiErrorException
import com.easipos.mobileordering.util.ErrorUtil.GENERIC_API_ERROR_EXCEPTION
import com.easipos.mobileordering.util.ErrorUtil.TIMEOUT_ERROR_EXCEPTION
import com.orhanobut.logger.Logger
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

private const val RESPONSE_OK = 200

abstract class BaseSingleObserverRetrofit<T : BaseResponseModel> : SingleObserver<T> {

    override fun onSubscribe(d: Disposable) {
    }

    override fun onSuccess(t: T) {
        try {
            Logger.i("onNext: Response code = " + t.code)

            if (t.code == RESPONSE_OK) {
                onResponseSuccess(t)
            } else {
                onFailure(ApiErrorException(t.message))
            }
        } catch (ex: Exception) {
            onFailure(ex)
        }
    }

    override fun onError(t: Throwable) {
        if (t is HttpException) {
            try {
                t.response()?.errorBody()?.let {
                    val model = Gson().fromJson(it.string(), EmptyResponseModel::class.java)
                    onFailure(ApiErrorException(model.message))
                    return
                }
            } catch (ex: Exception) {
            }
        } else if (t is ConnectException ||
                t is TimeoutException ||
                t is SocketTimeoutException ||
                t is UnknownHostException ||
                t is NoRouteToHostException) {
            onFailure(TIMEOUT_ERROR_EXCEPTION)
            return
        }

        onFailure(t)
    }

    abstract fun onResponseSuccess(responseModel: T)

    open fun onFailure(throwable: Throwable) {
        throwable.printStackTrace()
    }
}

abstract class EmptySingleObserverRetrofit : BaseSingleObserverRetrofit<EmptyResponseModel>() {

    override fun onResponseSuccess(responseModel: EmptyResponseModel) {
        onResponseSuccess()
    }

    abstract fun onResponseSuccess()
}

abstract class SingleObserverRetrofit<T> : BaseSingleObserverRetrofit<ResponseModel<T>>() {

    override fun onResponseSuccess(responseModel: ResponseModel<T>) {
        responseModel.data?.let {
            onResponseSuccess(responseModel.data)
        } ?: run {
            onFailure(GENERIC_API_ERROR_EXCEPTION)
        }

    }

    abstract fun onResponseSuccess(responseData: T)
}
