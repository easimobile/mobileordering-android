package com.easipos.mobileordering.api.responses.ordering

import com.google.gson.annotations.SerializedName

data class ProductResponseModel(
    @SerializedName("prod_cd")
    val prodCd: String?,

    @SerializedName("prod_sh_nm")
    val prodShNm: String?,

    @SerializedName("price")
    val price: String?,

    @SerializedName("prod_nm_ch")
    val prodNmCh: String?,

    @SerializedName("tax_1")
    val tax1: String?,

    @SerializedName("tax_2")
    val tax2: String?,

    @SerializedName("img_filepath")
    val imgFilePath: String?
)