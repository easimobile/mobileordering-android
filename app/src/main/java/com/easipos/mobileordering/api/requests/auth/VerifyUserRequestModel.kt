package com.easipos.mobileordering.api.requests.auth

data class VerifyUserRequestModel(
    val uid: String
)