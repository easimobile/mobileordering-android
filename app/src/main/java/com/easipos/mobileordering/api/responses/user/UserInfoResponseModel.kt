package com.easipos.mobileordering.api.responses.user

data class UserInfoResponseModel(
    val uid: String?,
    val profilePictureInBase64: String?,
    val name: String?,
    val email: String?,
    val phoneNumber: String?,
    val currency: String?
)