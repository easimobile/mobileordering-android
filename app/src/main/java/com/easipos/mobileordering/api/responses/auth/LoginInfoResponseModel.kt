package com.easipos.mobileordering.api.responses.auth

import com.google.gson.annotations.SerializedName

data class LoginInfoResponseModel(
    @SerializedName("token")
    val token: String?,

    @SerializedName("currency")
    val currency: String?
)