package com.easipos.mobileordering.executor

import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

interface DiskIOExecutor {
    fun getScheduler(): Executor
}

@Singleton
class IOExecutor @Inject constructor() : DiskIOExecutor {
    override fun getScheduler(): Executor {
        return Executors.newSingleThreadExecutor()
    }
}