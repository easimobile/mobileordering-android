package com.easipos.mobileordering.executor

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

interface ThreadExecutor {
    fun getScheduler(): Scheduler
}

@Singleton
class JobExecutor @Inject constructor() : ThreadExecutor {
    override fun getScheduler(): Scheduler {
        return Schedulers.io()
    }
}