package com.easipos.mobileordering.repositories.auth

import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.models.LoginInfo
import io.reactivex.Completable
import io.reactivex.Single

interface AuthRepository {

    fun verifyUser(model: VerifyUserRequestModel): Single<Boolean>

    fun login(model: LoginRequestModel): Single<LoginInfo>

    fun register(model: RegisterRequestModel): Single<String>

    fun logout(): Completable
}
