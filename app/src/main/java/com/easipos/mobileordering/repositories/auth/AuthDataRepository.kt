package com.easipos.mobileordering.repositories.auth

import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.datasource.DataFactory
import com.easipos.mobileordering.mapper.auth.LoginInfoMapper
import com.easipos.mobileordering.models.LoginInfo
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthDataRepository @Inject constructor(private val dataFactory: DataFactory,
                                             private val loginInfoMapper: LoginInfoMapper) :
    AuthRepository {

    override fun verifyUser(model: VerifyUserRequestModel): Single<Boolean> =
        dataFactory.createAuthDataSource()
            .verifyUser(model)

    override fun login(model: LoginRequestModel): Single<LoginInfo> =
        dataFactory.createAuthDataSource()
            .login(model, loginInfoMapper)

    override fun register(model: RegisterRequestModel): Single<String> =
        dataFactory.createAuthDataSource()
            .register(model)

    override fun logout(): Completable =
        dataFactory.createAuthDataSource()
            .logout()
}
