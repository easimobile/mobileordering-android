package com.easipos.mobileordering.repositories.precheck

import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.mobileordering.datasource.DataFactory
import io.reactivex.Single
import javax.inject.Inject

class PrecheckDataRepository @Inject constructor(private val dataFactory: DataFactory) : PrecheckRepository {

    override fun checkVersion(model: CheckVersionRequestModel): Single<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
