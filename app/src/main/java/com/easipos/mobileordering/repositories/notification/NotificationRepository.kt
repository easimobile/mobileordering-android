package com.easipos.mobileordering.repositories.notification

import com.easipos.mobileordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.mobileordering.api.requests.notification.RemoveFcmTokenRequestModel
import io.reactivex.Completable

interface NotificationRepository {

    fun registerFcmToken(model: RegisterFcmTokenRequestModel): Completable

    fun removeFcmToken(model: RemoveFcmTokenRequestModel): Completable
}
