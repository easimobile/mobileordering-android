package com.easipos.mobileordering.repositories.user

import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.datasource.DataFactory
import com.easipos.mobileordering.mapper.user.UserInfoMapper
import com.easipos.mobileordering.models.UserInfo
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserDataRepository @Inject constructor(private val dataFactory: DataFactory,
                                             private val userInfoMapper: UserInfoMapper) :
    UserRepository {

    override fun getUserInfo(): Single<UserInfo> =
        dataFactory.createUserDataSource()
            .getUserInfo(userInfoMapper)

    override fun updateProfile(model: UpdateProfileRequestModel): Observable<Void> =
        dataFactory.createUserDataSource()
            .updateProfile(model)
}
