package com.easipos.mobileordering.repositories.precheck

import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import io.reactivex.Single

interface PrecheckRepository {

    fun checkVersion(model: CheckVersionRequestModel): Single<Boolean>
}
