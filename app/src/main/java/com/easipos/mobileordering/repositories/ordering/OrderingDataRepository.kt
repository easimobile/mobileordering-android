package com.easipos.mobileordering.repositories.ordering

import com.easipos.mobileordering.api.requests.ordering.*
import com.easipos.mobileordering.datasource.DataFactory
import com.easipos.mobileordering.mapper.ordering.MenuMapper
import com.easipos.mobileordering.mapper.ordering.ProductMapper
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Menu
import com.easipos.mobileordering.models.Product
import com.easipos.mobileordering.models.Remark
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class OrderingDataRepository @Inject constructor(private val dataFactory: DataFactory,
                                                 private val menuMapper: MenuMapper,
                                                 private val productMapper: ProductMapper) :
    OrderingRepository {


    override fun getMenu(): Single<List<Menu>> =
        dataFactory.createOrderingDataSource()
            .getMenu(menuMapper)

    override fun getNewProducts(model: GetNewProductsRequestModel): Single<List<Product>> =
        dataFactory.createOrderingDataSource()
            .getNewProducts(model, productMapper)

    override fun insertCartItem(cartItem: CartItem): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .insertCartItem(cartItem)

    override fun updateCartItemQuantity(cartItem: CartItem): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .updateCartItemQuantity(cartItem)

    override fun removeCartItem(cartItem: CartItem): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .removeCartItem(cartItem)

    override fun getOrderItems(model: GetOrderItemsRequestModel): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .getOrderItems(model)

    override fun voidItem(model: VoidItemRequestModel): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .voidItem(model)

    override fun getRemarks(model: GetRemarksRequestModel): Single<List<Remark>> =
        dataFactory.createOrderingDataSource()
            .getRemarks(model)

    override fun submitRemarks(remarks: List<Remark>): Single<List<CartItem>> =
        dataFactory.createOrderingDataSource()
            .submitRemarks(remarks)

    override fun holdBill(model: HoldBillRequestModel): Observable<Void> =
        dataFactory.createOrderingDataSource()
            .holdBill(model)

    override fun getGuestCheck(model: GetGuestCheckRequestModel): Single<List<String>> =
        dataFactory.createOrderingDataSource()
            .getGuestCheck(model)
}
