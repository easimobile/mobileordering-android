package com.easipos.mobileordering.repositories.user

import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.models.UserInfo
import io.reactivex.Observable
import io.reactivex.Single

interface UserRepository {

    fun getUserInfo(): Single<UserInfo>

    fun updateProfile(model: UpdateProfileRequestModel): Observable<Void>
}
