package com.easipos.mobileordering.event_bus

data class NotificationCount(val count: Int)