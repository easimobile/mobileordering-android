package com.easipos.mobileordering.services

import android.os.AsyncTask
import com.easipos.mobileordering.api.requests.notification.RegisterFcmTokenRequestModel
import com.easipos.mobileordering.api.requests.notification.RemoveFcmTokenRequestModel
import com.easipos.mobileordering.managers.UserManager
import com.easipos.mobileordering.tools.Preference
import com.easipos.mobileordering.use_cases.base.DefaultCompletableObserver
import com.easipos.mobileordering.use_cases.notification.RegisterFcmTokenUseCase
import com.easipos.mobileordering.use_cases.notification.RemoveFcmTokenUseCase
import com.orhanobut.logger.Logger
import java.io.IOException
import javax.inject.Inject

class FcmService @Inject constructor(private val registerFcmTokenUseCase: RegisterFcmTokenUseCase,
                                     private val removeFcmTokenUseCase: RemoveFcmTokenUseCase) {

    fun saveFcmToken(fcmToken: String) {
        Logger.d("saveFcmToken: $fcmToken")

        synchronized(this) {
            Preference.prefFcmToken = fcmToken

            // Register FCM Token in MainActivity
            Preference.prefIsFcmTokenRegistered = false

            registerFcmToken()
        }
    }

    fun registerFcmToken() {
        if (Preference.prefIsLoggedIn) {
            val fcmToken = Preference.prefFcmToken

            if (fcmToken.isNotBlank()) {
                UserManager.token?.let {
                    Logger.d("registerFcmToken: Registering fcm token with {$fcmToken}")

                    val model = RegisterFcmTokenRequestModel(fcmToken)

                    registerFcmTokenUseCase.execute(object : DefaultCompletableObserver() {
                        override fun onComplete() {
                            Logger.i("registerFcmToken: Fcm token registered")
                            Preference.prefIsFcmTokenRegistered = true
                        }

                        override fun onError(error: Throwable) {
                            super.onError(error)
                            Logger.e("registerFcmToken: " + error.message)
                            Preference.prefIsFcmTokenRegistered = false
                        }
                    }, RegisterFcmTokenUseCase.Params.createQuery(model))
                }
            }
        }
    }

    fun removeFcmToken() {
        val fcmToken = Preference.prefFcmToken

        if (fcmToken.isNotBlank()) {
            Logger.d("removeFcmToken: Removing fcm token with {$fcmToken}")

            UserManager.token?.let {
                val model = RemoveFcmTokenRequestModel(fcmToken)

                removeFcmTokenUseCase.execute(object : DefaultCompletableObserver() {
                    override fun onComplete() {
                        Logger.i("removeFcmToken: Fcm token removed")
                    }

                    override fun onError(error: Throwable) {
                        super.onError(error)
                        Logger.e("removeFcmToken: " + error.message)
                    }
                }, RemoveFcmTokenUseCase.Params.createQuery(model))
            }
        }
    }

    fun resetFcmToken() {
        // Do not need to reset when using OneSignal
        //ResetFcmTokenAsyncTask().execute()
    }

    private class ResetFcmTokenAsyncTask : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg voids: Void): Void? {
            Logger.d("resetFcmToken: Deleting fcm instance id")
            try {
//                FirebaseInstanceId.getInstance().deleteInstanceId()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            return null
        }
    }
}