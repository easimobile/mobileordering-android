package com.easipos.mobileordering.base

import androidx.annotation.UiThread
import java.lang.ref.WeakReference

interface MvpPresenter<V : View> {
    @UiThread
    fun onAttachView(view: V)

    @UiThread
    fun onDetachView()
}

open class Presenter<V : View> : MvpPresenter<V> {

    private var viewRef: WeakReference<V>? = null

    var view: V? = null
        private set
        get() = viewRef?.get()

    @UiThread
    override fun onAttachView(view: V) {
        viewRef = WeakReference(view)
    }

    @UiThread
    override fun onDetachView() {
        if (viewRef != null) {
            viewRef!!.clear()
            viewRef = null
        }
    }

    @UiThread
    fun isViewAttached(): Boolean {
        return viewRef != null && viewRef!!.get() != null
    }
}