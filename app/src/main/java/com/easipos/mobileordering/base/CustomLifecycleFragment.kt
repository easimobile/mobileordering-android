package com.easipos.mobileordering.base

import com.easipos.mobileordering.di.components.FragmentComponent
import io.github.anderscheow.library.appCompat.fragment.LifecycleFragment
import io.github.anderscheow.library.constant.EventBusType
import io.github.anderscheow.library.viewModel.BaseAndroidViewModel

abstract class CustomLifecycleFragment<VM : BaseAndroidViewModel<*>> : LifecycleFragment<VM>() {

    var component: FragmentComponent? = null
        private set

    abstract fun instantiateComponent(): FragmentComponent

    open fun initInjection() {
        if (component == null) {
            component = instantiateComponent()
        }
    }

    override fun getEventBusType(): EventBusType? = null

    override fun initAfterOnAttach() {
        initInjection()
    }
}
