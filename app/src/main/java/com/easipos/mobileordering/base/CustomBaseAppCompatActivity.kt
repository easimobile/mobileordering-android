package com.easipos.mobileordering.base

import android.content.Context
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.di.components.ActivityComponent
import io.github.anderscheow.library.appCompat.activity.BaseAppCompatActivity

abstract class CustomBaseAppCompatActivity : BaseAppCompatActivity() {

    var component: ActivityComponent? = null
        private set

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(Easi.localeManager.setLocale(base))
    }

    abstract fun instantiateComponent(): ActivityComponent

    open fun initInjection() {
        if (component == null) {
            component = instantiateComponent()
        }
    }

    override fun initBeforeSuperOnCreate() {
        initInjection()
    }
}
