package com.easipos.mobileordering.tools

import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_IS_FCM_REGISTERED = "PREF_IS_FCM_REGISTERED"
    private const val PREF_FCM_TOKEN = "PREF_FCM_TOKEN"
    private const val PREF_NOTIFICATION_COUNT = "PREF_NOTIFICATION_COUNT"
    private const val PREF_TABLE_NAME = "PREF_TABLE_NAME"
    private const val PREF_TABLE_SYS_ID = "PREF_TABLE_SYS_ID"
    private const val PREF_CURRENCY = "PREF_CURRENCY"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefIsFcmTokenRegistered: Boolean
        get() = Prefs.getBoolean(PREF_IS_FCM_REGISTERED, false)
        set(isRegistered) = Prefs.putBoolean(PREF_IS_FCM_REGISTERED, isRegistered)

    var prefFcmToken: String
        get() = Prefs.getString(PREF_FCM_TOKEN, "")
        set(token) = Prefs.putString(PREF_FCM_TOKEN, token)

    var prefNotificationCount: Int
        get() = Prefs.getInt(PREF_NOTIFICATION_COUNT, 0)
        set(count) = Prefs.putInt(PREF_NOTIFICATION_COUNT, count)

    var prefTableName: String
        get() = Prefs.getString(PREF_TABLE_NAME, "")
        set(tableName) = Prefs.putString(PREF_TABLE_NAME, tableName)

    var prefTableSysId: String
        get() = Prefs.getString(PREF_TABLE_SYS_ID, "")
        set(tableSysId) = Prefs.putString(PREF_TABLE_SYS_ID, tableSysId)

    var prefCurrency: String
        get() = Prefs.getString(PREF_CURRENCY, "")
        set(currency) = Prefs.putString(PREF_CURRENCY, currency)

    fun logout() {
        prefIsLoggedIn = false
        prefIsFcmTokenRegistered = false
        prefNotificationCount = 0
        prefTableName = ""
        prefTableSysId = ""
        prefCurrency = ""

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}