package com.easipos.mobileordering.tools

import android.graphics.Bitmap
import android.graphics.Canvas
import android.view.View
import android.view.animation.*
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatImageView
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.library.kotlinExt.visible

class AddToCartAnimation(private val refView: AppCompatImageView,
                         private val fromView: AppCompatImageView,
                         private val toView: View,
                         private val listener: AnimationListener) {

    companion object {
        private const val DURATION = 650L
        private const val START_OFFSET = 100L
    }

    interface AnimationListener {
        fun onComplete()
    }

    private var isStarted = false

    fun start() {
        if (isStarted) return
        isStarted = true

        fromView.visible()
        fromView.alpha = 1f

        val viewPosition = IntArray(2)
        refView.getLocationOnScreen(viewPosition)

        fromView.apply {
            this.setImageBitmap(drawViewToBitmap(refView, refView.width, refView.height))
            this.layoutParams = FrameLayout.LayoutParams(refView.width, refView.height).apply {
                this.marginStart = viewPosition[0]
                this.topMargin = viewPosition[1]
            }
        }

        val animator = AnimationSet(false)

        val scale = ScaleAnimation(1f, 0f, 1f, 0f,
            Animation.RELATIVE_TO_SELF, .5f,
            Animation.RELATIVE_TO_SELF, .5f).apply {
            this.duration = DURATION + START_OFFSET
        }

        val viewX = refView.x
        val viewY = refView.y
        val toX = toView.x - viewPosition[0] - refView.width / 3f
        val toY = toView.y - viewPosition[1] - refView.height / 2f

        val translation = TranslateAnimation(viewX, toX, viewY, toY).apply {
            this.startOffset = START_OFFSET
            this.duration = DURATION
        }

        val alpha = AlphaAnimation(1f, 0f).apply {
            this.startOffset = START_OFFSET * 2
            this.duration = DURATION
            this.interpolator = DecelerateInterpolator()
        }

        animator.apply {
            this.addAnimation(scale)
            this.addAnimation(translation)
            this.addAnimation(alpha)
            this.fillAfter = true
            this.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {
                }

                override fun onAnimationEnd(animation: Animation?) {
                    fromView.gone()
                    listener.onComplete()
                    isStarted = false
                }

                override fun onAnimationStart(animation: Animation?) {
                }
            })
        }

        fromView.startAnimation(animator)
    }

    private fun drawViewToBitmap(view: View, width: Int, height: Int): Bitmap {
        val dest = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val c = Canvas(dest)
        view.draw(c)
        return dest
    }
}