package com.easipos.mobileordering.mapper.ordering

import com.easipos.mobileordering.api.responses.ordering.MenuResponseModel
import com.easipos.mobileordering.mapper.Mapper
import com.easipos.mobileordering.models.Menu
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MenuMapper @Inject constructor() : Mapper<Menu, MenuResponseModel>() {

    override fun transform(item: MenuResponseModel): Menu {
        return Menu(
            deptCd = item.deptCd ?: "",
            deptNm = item.deptNm ?: "",
            deptCh = item.deptCh ?: "",
            dispOrder = item.dispOrder ?: ""
        )
    }
}