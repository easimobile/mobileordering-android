package com.easipos.mobileordering.mapper.user

import com.easipos.mobileordering.api.responses.user.UserInfoResponseModel
import com.easipos.mobileordering.mapper.Mapper
import com.easipos.mobileordering.models.UserInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserInfoMapper @Inject constructor() : Mapper<UserInfo, UserInfoResponseModel>() {

    override fun transform(item: UserInfoResponseModel): UserInfo {
        return UserInfo(
            uid = item.uid ?: "",
            profilePictureInBase64 = item.profilePictureInBase64,
            name = item.name ?: "",
            email = item.email ?: "",
            phoneNumber = item.phoneNumber ?: "",
            currency = item.currency ?: ""
        )
    }
}