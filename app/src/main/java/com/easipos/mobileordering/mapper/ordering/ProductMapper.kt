package com.easipos.mobileordering.mapper.ordering

import com.easipos.mobileordering.api.responses.ordering.ProductResponseModel
import com.easipos.mobileordering.mapper.Mapper
import com.easipos.mobileordering.models.Product
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductMapper @Inject constructor() : Mapper<Product, ProductResponseModel>() {

    override fun transform(item: ProductResponseModel): Product {
        return Product(
            prodCd = item.prodCd ?: "",
            prodShNm = item.prodShNm ?: "",
            price = item.price ?: "",
            prodNmCh = item.prodNmCh ?: "",
            tax1 = item.tax1 ?: "",
            tax2 = item.tax2 ?: "",
            imgFilePath = item.imgFilePath ?: ""
        )
    }
}