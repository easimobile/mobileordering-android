package com.easipos.mobileordering.mapper.auth

import com.easipos.mobileordering.api.responses.auth.LoginInfoResponseModel
import com.easipos.mobileordering.mapper.Mapper
import com.easipos.mobileordering.models.LoginInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginInfoMapper @Inject constructor() : Mapper<LoginInfo, LoginInfoResponseModel>() {

    override fun transform(item: LoginInfoResponseModel): LoginInfo {
        return LoginInfo(
            token = item.token ?: "",
            currency = item.currency ?: ""
        )
    }
}