package com.easipos.mobileordering.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Remark

@Dao
interface OrderingDao {

    //region CartItem
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartItem(cartItem: CartItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCartItems(cartItems: List<CartItem>)

    @Query("SELECT * FROM CartItem WHERE id = :id AND prodCd = :prodCd AND isRemovable = 1")
    fun findCartItemIsRemovable(id: Int, prodCd: String): CartItem?

    @Query("SELECT * FROM CartItem ORDER BY isRemovable ASC")
    fun findCartItems(): List<CartItem>

    @Query("SELECT * FROM CartItem WHERE isRemovable = 1 ORDER BY isRemovable ASC")
    fun findCartItemsIsRemovable(): List<CartItem>

    @Query("UPDATE CartItem SET quantity = :quantity WHERE id = :id")
    fun updateCartItemQuantity(quantity: Int, id: Int)

    @Query("UPDATE CartItem SET remarks = :remarks WHERE id = :id")
    fun updateCartItemRemark(remarks: String, id: Int)

    @Query("DELETE FROM CartItem WHERE id = :id")
    fun removeCartItemById(id: Int)

    @Query("DELETE FROM CartItem WHERE prodCd = :prodCd AND linkCd = :linkCd")
    fun removeCartItemByLinkCd(prodCd: String, linkCd: String)

    @Query("DELETE FROM CartItem WHERE isRemovable = 0")
    fun removeCartItemByIsNotRemovable()

    @Query("DELETE FROM CartItem")
    fun removeAllCartItems()
    //endregion

    //region Remark
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRemarks(remarks: List<Remark>)

    @Query("UPDATE REMARK SET isSelected = :isSelected WHERE id = :id")
    fun updateRemark(id: Int, isSelected: Boolean)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCustomRemark(remark: Remark)

    @Query("SELECT * FROM Remark")
    fun findRemarks(): List<Remark>

    @Query("SELECT * FROM Remark WHERE cartItemId = :cartItemId AND prodCode = :prodCode AND isSelected = 1")
    fun findRemarksIsSelected(cartItemId: Int, prodCode: String): List<Remark>

    @Query("DELETE FROM Remark WHERE cartItemId = :cartItemId AND prodCode = :prodCode")
    fun removeRemarksByCartItemIdAndProdCode(cartItemId: Int, prodCode: String)

    @Query("DELETE FROM Remark")
    fun removeAllRemarks()
    //endregion
}