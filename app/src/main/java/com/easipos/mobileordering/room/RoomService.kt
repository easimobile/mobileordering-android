package com.easipos.mobileordering.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.easipos.mobileordering.BuildConfig
import com.easipos.mobileordering.models.CartItem
import com.easipos.mobileordering.models.Notification
import com.easipos.mobileordering.models.Remark

@Database(
        entities = [
            CartItem::class,
            Remark::class,
            Notification::class
        ],
        version = BuildConfig.DB_VERSION,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomService : RoomDatabase() {

    abstract fun orderingDao(): OrderingDao

    abstract fun notificationDao(): NotificationDao
}
