package com.easipos.mobileordering.activities.register.navigation

import android.app.Activity

interface RegisterBaseNavigation {

    fun navigateToMain(activity: Activity)
}