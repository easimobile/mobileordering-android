package com.easipos.mobileordering.activities.change_password.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

abstract class ChangePasswordBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, ChangePasswordNavigation