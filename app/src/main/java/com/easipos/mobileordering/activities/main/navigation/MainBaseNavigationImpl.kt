package com.easipos.mobileordering.activities.main.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

abstract class MainBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, MainNavigation