package com.easipos.mobileordering.activities.change_password.mvp

import android.app.Application
import com.easipos.mobileordering.R
import com.easipos.mobileordering.base.Presenter
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseUser

abstract class ChangePasswordBasePresenter(private val application: Application)
    : Presenter<ChangePasswordView>() {

    fun doReauthenticateUser(currentPassword: String, newPassword: String) {
        FirebaseAuth.getInstance().currentUser?.let { user ->
            view?.setLoadingIndicator(true)
            val credential = EmailAuthProvider
                .getCredential(user.email ?: "", currentPassword)
            user.reauthenticate(credential)
                .addOnCompleteListener { task ->
                    view?.setLoadingIndicator(false)
                    if (task.isSuccessful) {
                        doChangePassword(user, newPassword)
                    } else {
                        task.exception?.let { exception ->
                            exception.printStackTrace()
                            if (exception is FirebaseAuthInvalidCredentialsException) {
                                // Invalid current password
                                view?.showErrorAlertDialog(application.getString(R.string.error_invalid_current_password)) {
                                    view?.resetFields()
                                }
                            } else {
                                view?.showErrorAlertDialog(exception.localizedMessage ?: "") {
                                    view?.resetFields()
                                }
                            }
                        }
                    }
                }
        }
    }

    private fun doChangePassword(user: FirebaseUser, newPassword: String) {
        view?.setLoadingIndicator(true)
        user.updatePassword(newPassword)
            .addOnCompleteListener { task ->
                view?.setLoadingIndicator(false)
                if (task.isSuccessful) {
                    view?.finishScreen()
                    view?.toastMessage(R.string.prompt_password_change_successful)
                } else {
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }
}
