package com.easipos.mobileordering.activities.change_password

import android.widget.TextView
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.change_password.mvp.ChangePasswordPresenter
import com.easipos.mobileordering.activities.change_password.mvp.ChangePasswordView
import com.easipos.mobileordering.activities.change_password.navigation.ChangePasswordNavigation
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import io.github.anderscheow.library.kotlinExt.clearTexts
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.EqualRule
import io.github.anderscheow.validator.rules.common.MinRule
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_change_password.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

abstract class ChangePasswordBaseActivity : CustomBaseAppCompatActivity(), ChangePasswordView {

    //region Variables
    @Inject
    lateinit var presenter: ChangePasswordPresenter

    @Inject
    lateinit var navigation: ChangePasswordNavigation

    @Inject
    lateinit var validator: Validator

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptChangePassword()
        true
    }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
        DaggerActivityComponent.builder()
            .appComponent((application as Easi).getAppComponent())
            .activityModule(ActivityModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.activity_change_password

    override fun init() {
        setupListeners()
    }
    //endregion

    //region LoginView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun resetFields() {
        clearTexts(text_input_edit_text_password, text_input_edit_text_new_password, text_input_edit_text_confirm_new_password)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action methods
    private fun setupListeners() {
        text_input_edit_text_confirm_new_password.setOnEditorActionListener(onEditorActionListener)

        image_view_back.setOnClickListener {
            finish()
        }

        button_update.setOnClickListener {
            attemptChangePassword()
        }
    }

    private fun attemptChangePassword() {
        val currentPasswordValidation = Validation(text_input_layout_password)
            .add(NotBlankRule(R.string.error_field_required))
        val newPasswordValidation = Validation(text_input_layout_new_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
        val confirmNewPasswordValidation = Validation(text_input_layout_confirm_new_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
            .add(EqualRule(text_input_edit_text_new_password.text?.toString() ?: "", R.string.error_password_not_match))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val currentPassword = values[0].trim()
                val newPassword = values[1].trim()

                presenter.doReauthenticateUser(currentPassword, newPassword)
            }
        }).validate(currentPasswordValidation, newPasswordValidation, confirmNewPasswordValidation)
    }
}
