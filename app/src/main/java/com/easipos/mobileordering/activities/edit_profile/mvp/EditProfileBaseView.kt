package com.easipos.mobileordering.activities.edit_profile.mvp

import com.easipos.mobileordering.base.View
import com.easipos.mobileordering.models.UserInfo

interface EditProfileBaseView : View {

    fun populateUserInfo(userInfo: UserInfo)

    fun refreshUserInfo()

    fun finishScreen()
}
