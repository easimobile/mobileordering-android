package com.easipos.mobileordering.activities.register

import android.widget.TextView
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.register.mvp.RegisterPresenter
import com.easipos.mobileordering.activities.register.mvp.RegisterView
import com.easipos.mobileordering.activities.register.navigation.RegisterNavigation
import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.bundle.ParcelData
import com.easipos.mobileordering.constant.AuthType
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import com.google.firebase.auth.FirebaseUser
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.gone
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.EqualRule
import io.github.anderscheow.validator.rules.common.MinRule
import io.github.anderscheow.validator.rules.common.NotBlankRule
import io.github.anderscheow.validator.rules.regex.EmailRule
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

abstract class RegisterBaseActivity : CustomBaseAppCompatActivity(), RegisterView {

    //region Variables
    @Inject
    lateinit var presenter: RegisterPresenter

    @Inject
    lateinit var navigation: RegisterNavigation

    @Inject
    lateinit var validator: Validator

    private val firebaseUser by argument<FirebaseUser>(ParcelData.FIREBASE_USER)
    private val authType by argument(ParcelData.AUTH_TYPE, AuthType.PASSWORD)

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptRegister()
        true
    }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
        DaggerActivityComponent.builder()
            .appComponent((application as Easi).getAppComponent())
            .activityModule(ActivityModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.activity_register

    override fun init() {
        setupViews()
        setupListeners()
        populateFirebaseUser()
    }
    //endregion

    //region RegisterBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action methods
    private fun setupViews() {
        if (authType != AuthType.PASSWORD) {
            text_input_layout_password.gone()
            text_input_layout_confirm_password.gone()
        }
    }

    private fun setupListeners() {
        text_input_edit_text_confirm_password.setOnEditorActionListener(onEditorActionListener)

        image_view_back.setOnClickListener {
            finish()
        }

        button_register.setOnClickListener {
            attemptRegister()
        }
    }

    private fun populateFirebaseUser() {
        firebaseUser?.let {
            Logger.d("${it.photoUrl}, ${it.phoneNumber}")
            // Todo: Data from FirebaseUser, photoUrl and phoneNumber reversed
            text_input_edit_text_name.setText(it.displayName ?: "")
            text_input_edit_text_email.setText(it.email ?: "")
        }
    }

    private fun attemptRegister() {
        val nameValidation = Validation(text_input_layout_name)
            .add(NotBlankRule(R.string.error_field_required))
        val emailValidation = Validation(text_input_layout_email)
            .add(NotBlankRule(R.string.error_field_required))
            .add(EmailRule(R.string.error_invalid_email_format))
        val passwordValidation = Validation(text_input_layout_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
        val confirmPasswordValidation = Validation(text_input_layout_confirm_password)
            .add(NotBlankRule(R.string.error_field_required))
            .add(MinRule(6, R.string.error_password_min_length))
            .add(EqualRule(text_input_edit_text_password.text?.toString() ?: "", R.string.error_password_not_match))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val name = values[0].trim()
                val email = values[1].trim()
                val password = values.getOrNull(2)?.trim() ?: ""

                checkAuthType(name, email, password)
            }
        }).apply {
            if (authType == AuthType.PASSWORD) {
                this.validate(
                    nameValidation, emailValidation, passwordValidation, confirmPasswordValidation
                )
            } else {
                this.validate(
                    nameValidation, emailValidation
                )
            }
        }
    }

    private fun checkAuthType(name: String, email: String, password: String) {
        val model = RegisterRequestModel(
            name = name,
            email = email
        )
        if (authType == AuthType.PASSWORD) {
            presenter.doCheckPasswordAuthenticationExisted(model, password)
        } else {
            if (firebaseUser != null) {
                presenter.doRegister(model)
            }
        }
    }
}
