package com.easipos.mobileordering.activities.change_password.mvp

import com.easipos.mobileordering.base.View

interface ChangePasswordBaseView : View {

    fun resetFields()

    fun finishScreen()
}
