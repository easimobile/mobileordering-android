package com.easipos.mobileordering.activities.main

import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.main.mvp.MainPresenter
import com.easipos.mobileordering.activities.main.mvp.MainView
import com.easipos.mobileordering.activities.main.navigation.MainNavigation
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.mobileordering.managers.FcmManager
import com.easipos.mobileordering.managers.PushNotificationManager
import com.orhanobut.logger.Logger
import org.jetbrains.anko.toast
import javax.inject.Inject

abstract class MainBaseActivity : CustomBaseAppCompatActivity(), MainView {

    //region Variables
    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var navigation: MainNavigation

    @Inject
    lateinit var fcmManager: FcmManager

    @Inject
    lateinit var pushNotificationManager: PushNotificationManager
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_main

    override fun init() {
        super.init()
//        registerPushTokenIfPossible()
//        processPayload()
        setupViews()
        setupListeners()
    }
    //endregion

    //region MainBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        toast(message)
    }

    override fun toastMessage(message: Int) {
        toast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }
    //endregion

    //region Interface methods

    //endregion

    //region Action Methods
    fun processPayload() {
        Logger.d(pushNotificationManager.payload)
        pushNotificationManager.payload?.let { _ ->
//            navigateToNotification()
            pushNotificationManager.removePayload()
        }
    }

    private fun registerPushTokenIfPossible() {
        fcmManager.service.registerFcmToken()
    }

    private fun setupViews() {

    }

    private fun setupListeners() {

    }
    //endregion
}
