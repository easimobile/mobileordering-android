package com.easipos.mobileordering.activities.splash

import android.os.AsyncTask
import androidx.appcompat.widget.Toolbar
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.splash.mvp.SplashPresenter
import com.easipos.mobileordering.activities.splash.mvp.SplashView
import com.easipos.mobileordering.activities.splash.navigation.SplashNavigation
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.bundle.ParcelData
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.room.RoomService
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.delay
import io.github.anderscheow.library.kotlinExt.rate
import javax.inject.Inject

abstract class SplashBaseActivity : CustomBaseAppCompatActivity(), SplashView {

    //region Variables
    @Inject
    lateinit var presenter: SplashPresenter

    @Inject
    lateinit var navigation: SplashNavigation

    @Inject
    lateinit var roomService: RoomService

    private val clearDb by argument(ParcelData.CLEAR_DB, true)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
            DaggerActivityComponent.builder()
                    .appComponent((application as Easi).getAppComponent())
                    .activityModule(ActivityModule(this))
                    .build()

    override fun getResLayout(): Int = R.layout.activity_splash

    override fun init() {
        forceLogout()

        if (clearDb) {
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.clearAllTables()
                }
            }
        }

        delay(1000) {
            navigation.navigateToLogin(this)
        }
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashBaseActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
    }

    override fun navigateToMain() {
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }

    private fun forceLogout() {
        (application as? Easi)?.logout()
    }
    //endregion
}
