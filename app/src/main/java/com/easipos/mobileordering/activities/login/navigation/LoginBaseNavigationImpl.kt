package com.easipos.mobileordering.activities.login.navigation

import android.app.Activity
import android.view.View
import com.easipos.mobileordering.activities.main.MainActivity
import com.easipos.mobileordering.activities.register.RegisterActivity
import com.easipos.mobileordering.activities.reset_password.ResetPasswordActivity
import com.easipos.mobileordering.bundle.RequestCode
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.extensions.LayoutContainer

abstract class LoginBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, LoginNavigation {

    override fun navigateToRegister(activity: Activity, firebaseUser: FirebaseUser?, authType: String) {
        activity.startActivityForResult(RegisterActivity.newIntent(activity, firebaseUser, authType), RequestCode.FINISH_LOGIN_RC)
    }

    override fun navigateToForgotPassword(activity: Activity) {
        activity.startActivity(ResetPasswordActivity.newIntent(activity))
    }

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}