package com.easipos.mobileordering.activities.splash.navigation

import android.app.Activity

interface SplashBaseNavigation {

    fun navigateToLogin(activity: Activity)
}