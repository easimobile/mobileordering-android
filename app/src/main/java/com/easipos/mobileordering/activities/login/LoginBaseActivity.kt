package com.easipos.mobileordering.activities.login

import android.app.Activity
import android.content.Intent
import android.util.Base64
import android.widget.TextView
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.login.mvp.LoginPresenter
import com.easipos.mobileordering.activities.login.mvp.LoginView
import com.easipos.mobileordering.activities.login.navigation.LoginNavigation
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.bundle.RequestCode
import com.easipos.mobileordering.constant.AuthType
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.nets.enets.exceptions.InvalidPaymentRequestException
import com.nets.enets.listener.PaymentCallback
import com.nets.enets.network.PaymentRequestManager
import com.nets.enets.utils.result.DebitCreditPaymentResponse
import com.nets.enets.utils.result.NETSError
import com.nets.enets.utils.result.NonDebitCreditPaymentResponse
import com.nets.enets.utils.result.PaymentResponse
import com.orhanobut.logger.Logger
import io.github.anderscheow.library.kotlinExt.formatDate
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import io.github.anderscheow.validator.rules.regex.EmailRule
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.longToast
import java.security.MessageDigest
import java.util.*
import javax.inject.Inject

abstract class LoginBaseActivity : CustomBaseAppCompatActivity(), LoginView {

    companion object {
        private const val KEY_ID = "154eb31c-0f72-45bb-9249-84a1036fd1ca"
        private const val SECRET_KEY = "38a4b473-0295-439d-92e1-ad26a8c60279"
        private const val UMID = "UMID_877772003"
    }

    //region Variables
    @Inject
    lateinit var presenter: LoginPresenter

    @Inject
    lateinit var navigation: LoginNavigation

    @Inject
    lateinit var validator: Validator

    private var callbackManager: CallbackManager? = null

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptPasswordAuth()
        true
    }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RequestCode.FINISH_LOGIN_RC -> {
                if (resultCode == Activity.RESULT_OK) {
                    finish()
                }
            }

            RequestCode.GOOGLE_SIGN_IN_RC -> handleGoogleSignIn(data)

            else -> {
                callbackManager?.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
        DaggerActivityComponent.builder()
            .appComponent((application as Easi).getAppComponent())
            .activityModule(ActivityModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.activity_login

    override fun init() {
        super.init()
        setupListeners()
    }
    //endregion

    //region LoginBaseView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message, true)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun navigateToRegister(firebaseUser: FirebaseUser?, authType: String) {
        navigation.navigateToRegister(this, firebaseUser, authType)
    }

    override fun navigateToMain() {
        navigation.navigateToMain(this)
    }
    //endregion

    //region Action methods
    private fun setupListeners() {
        text_input_edit_text_password.setOnEditorActionListener(onEditorActionListener)

        text_view_forgot_password.setOnClickListener {
            navigation.navigateToForgotPassword(this)
        }

        button_login.setOnClickListener {
            attemptPasswordAuth()
        }

        button_create_account.setOnClickListener {
            navigation.navigateToRegister(this)
        }

        image_view_facebook.setOnClickListener {
            doFacebookLogin()
        }

        image_view_google.setOnClickListener {
            doGoogleLogin()
        }

        image_view_face_login.setOnClickListener {

        }
    }

    private fun doFacebookLogin() {
        setLoadingIndicator(true)
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().apply {
            this.logInWithReadPermissions(this@LoginBaseActivity, arrayListOf("email", "public_profile"))
            this.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    setLoadingIndicator(false)
                    handleFacebookAccessToken(loginResult.accessToken)
                }

                override fun onCancel() {
                    setLoadingIndicator(false)
                }

                override fun onError(error: FacebookException) {
                    setLoadingIndicator(false)
                    error.printStackTrace()
                    toastMessage(R.string.error_facebook_login)
                }
            })
        }
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        val credential = FacebookAuthProvider.getCredential(token.token)
        presenter.doFirebaseSignIn(credential, AuthType.FACEBOOK)
    }

    private fun doGoogleLogin() {
        setLoadingIndicator(true)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleSignInClient = GoogleSignIn.getClient(this, gso)

        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RequestCode.GOOGLE_SIGN_IN_RC)
    }

    private fun handleGoogleSignIn(data: Intent?) {
        Logger.d("handleGoogleSignIn")
        setLoadingIndicator(false)
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            // Google Sign In was successful, authenticate with Firebase
            val account = task.getResult(ApiException::class.java)!!
            val credential = GoogleAuthProvider.getCredential(account.idToken, null)
            presenter.doFirebaseSignIn(credential, AuthType.GOOGLE)
        } catch (ex: ApiException) {
            ex.printStackTrace()
            toastMessage(R.string.error_google_login)
        }
    }

    private fun attemptPasswordAuth() {
        val emailValidation = Validation(text_input_layout_email)
            .add(NotBlankRule(R.string.error_field_required))
            .add(EmailRule(R.string.error_invalid_email_format))
        val passwordValidation = Validation(text_input_layout_password)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val email = values[0].trim()
                val password = values[1].trim()

                presenter.doPasswordAuth(email, password)
            }
        }).validate(emailValidation, passwordValidation)
    }

    private fun doPayment() {
        val dateTime = Calendar.getInstance().timeInMillis.formatDate("yyyyMMdd HH:mm:ss.SSS")
        val txn = "{\n" +
                "  \"ss\": \"1\",\n" +
                "  \"msg\": {\n" +
                "    \"netsMid\": \"$UMID\",\n" +
                "    \"submissionMode\": \"B\",\n" +
                "    \"txnAmount\": \"99\",\n" +
                "    \"merchantTxnRef\": \"$dateTime\",\n" +
                "    \"merchantTxnDtm\": \"$dateTime\",\n" +
                "    \"paymentType\": \"SALE\",\n" +
                "    \"currencyCode\": \"SGD\",\n" +
                "    \"merchantTimeZone\": \"+8:00\",\n" +
                "    \"b2sTxnEndURL\": \"https://httpbin.org/post\",\n" +
                "    \"b2sTxnEndURLParam\": \"\",\n" +
                "    \"s2sTxnEndURL\": \"http://s2s-parser.requestcatcher.com/test\",\n" +
                "    \"s2sTxnEndURLParam\": \"\",\n" +
                "    \"clientType\": \"S\",\n" +
                "    \"netsMidIndicator\": \"U\",\n" +
                "    \"ipAddress\": \"172.0.0.1\",\n" +
                "    \"language\": \"en\",\n" +
                "    \"ss\": \"1\",\n" +
                "    \"mobileOS\": \"ANDROID\"\n" +
                "  }\n" +
                "}"
        val hmac = generateSignature(txn, SECRET_KEY)

        val paymentCallback = getDefaultPaymentCallback()

        val manager = PaymentRequestManager.getSharedInstance()
        try {
            manager.sendPaymentRequest(
                KEY_ID,
                hmac,
                txn,
                paymentCallback,
                this
            )
        } catch (e: InvalidPaymentRequestException) {
            // Do something with the exception
            e.printStackTrace()
        } catch (e: Exception) {
            // Do something with the exception
            e.printStackTrace()
        }
    }

    private fun getDefaultPaymentCallback(): PaymentCallback {
        return object : PaymentCallback {
            override fun onResult(response: PaymentResponse) {
                if (response is DebitCreditPaymentResponse) {
                    val txnResult = response.txnResp
                    val hmac = response.hmac
                    val keyId = response.keyId

                    //Do Something with the response data if needed
                    Logger.d("$txnResult, $hmac, $keyId")
                } else if (response is NonDebitCreditPaymentResponse) {
                    val txnStatus = response.status
                    val app = response.app

                    //Map the status code to some appropriate logic
                    Logger.d("$txnStatus, $app")
                }
            }

            override fun onFailure(error: NETSError) {
                val txnResponseCode = error.responeCode
                val txnActionCode = error.actionCode

                //Handle the error accordingly
                Logger.d("$txnResponseCode, $txnActionCode")
            }
        }
    }

    private fun generateSignature(txnReq: String, secretKey: String): String {
        val concatPayloadAndSecretKey = txnReq + secretKey
        return encodeBase64(hashSHA256ToBytes(concatPayloadAndSecretKey.toByteArray()))
    }

    private fun hashSHA256ToBytes(input: ByteArray): ByteArray {
        val md = MessageDigest.getInstance("SHA-256")
        md.update(input)

        return md.digest()
    }

    private fun encodeBase64(data: ByteArray): String {
        return Base64.encodeToString(data, Base64.NO_WRAP)
    }
    //endregion
}
