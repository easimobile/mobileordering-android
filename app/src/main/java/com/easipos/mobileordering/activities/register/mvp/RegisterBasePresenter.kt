package com.easipos.mobileordering.activities.register.mvp

import com.easipos.mobileordering.api.requests.auth.RegisterRequestModel
import com.easipos.mobileordering.base.Presenter
import com.easipos.mobileordering.managers.UserManager
import com.easipos.mobileordering.models.Auth
import com.easipos.mobileordering.use_cases.auth.RegisterUseCase
import com.easipos.mobileordering.use_cases.base.DefaultSingleObserver
import com.easipos.mobileordering.util.ErrorUtil
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.orhanobut.logger.Logger

abstract class RegisterBasePresenter(private val registerUseCase: RegisterUseCase)
    : Presenter<RegisterView>() {

    override fun onDetachView() {
        super.onDetachView()
        registerUseCase.dispose()
    }

    fun doCheckPasswordAuthenticationExisted(model: RegisterRequestModel, password: String) {
        view?.setLoadingIndicator(true)
        FirebaseAuth.getInstance().fetchSignInMethodsForEmail(model.email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    task.result?.signInMethods?.let { signInMethods ->
                        when {
                            signInMethods.isEmpty() -> {
                                doCreatePasswordAuthentication(model, password)
                            }

                            signInMethods.contains(EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD) -> {
                                doLoginPasswordAuthentication(model, password)
                            }

                            else -> {
                                view?.setLoadingIndicator(false)
                            }
                        }
                    }
                } else {
                    view?.setLoadingIndicator(false)
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    fun doRegister(model: RegisterRequestModel) {
        view?.setLoadingIndicator(true)
        getIdToken { idToken, user ->
            doRegister(idToken, user.uid, model)
        }
    }

    private fun doCreatePasswordAuthentication(model: RegisterRequestModel, password: String) {
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(model.email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    getIdToken { idToken, user ->
                        doRegister(idToken, user.uid, model)
                    }
                } else {
                    view?.setLoadingIndicator(false)
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    private fun doLoginPasswordAuthentication(model: RegisterRequestModel, password: String) {
        FirebaseAuth.getInstance().signInWithEmailAndPassword(model.email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    getIdToken { idToken, user ->
                        doRegister(idToken, user.uid, model)
                    }
                } else {
                    view?.setLoadingIndicator(false)
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    private fun doRegister(idToken: String, uid: String, model: RegisterRequestModel) {
        model.idToken = idToken
        model.uid = uid
        registerUseCase.execute(object : DefaultSingleObserver<String>() {
            override fun onSuccess(value: String) {
                view?.setLoadingIndicator(false)
                UserManager.token = Auth(value)
                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                // Only delete firebase user with email password sign in if register fail
                removeFirebaseUserForEmailPasswordSignIn()

                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, RegisterUseCase.Params.createQuery(model))
    }

    private fun getIdToken(action: (String, FirebaseUser) -> Unit) {
        FirebaseAuth.getInstance().currentUser?.apply {
            this.getIdToken(true).addOnCompleteListener {
                it.result?.token?.let { idToken ->
                    Logger.d(idToken)
                    action(idToken, this)
                } ?: run {
                    view?.setLoadingIndicator(false)
                }
            }
        }
    }

    private fun removeFirebaseUserForEmailPasswordSignIn() {
        FirebaseAuth.getInstance().currentUser?.providerData?.find { userInfo ->
            userInfo.providerId == EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD
        }?.let {
            FirebaseAuth.getInstance().currentUser?.delete()
        }
    }
}
