package com.easipos.mobileordering.activities.edit_profile.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

abstract class EditProfileBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, EditProfileNavigation