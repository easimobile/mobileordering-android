package com.easipos.mobileordering.activities.splash.mvp

import com.easipos.mobileordering.api.requests.precheck.CheckVersionRequestModel
import com.easipos.mobileordering.base.Presenter
import com.easipos.mobileordering.managers.UserManager
import com.easipos.mobileordering.tools.Preference
import com.easipos.mobileordering.use_cases.base.DefaultSingleObserver
import com.easipos.mobileordering.use_cases.precheck.CheckVersionUseCase

abstract class SplashBasePresenter (private val checkVersionUseCase: CheckVersionUseCase)
    : Presenter<SplashView>() {

    fun checkVersion() {
        checkVersionUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                super.onSuccess(value)
                if (value) {
                    view?.showUpdateAppDialog()
                } else {
                    checkIsAuthenticated()
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                checkIsAuthenticated()
            }
        }, CheckVersionUseCase.Params.createQuery(CheckVersionRequestModel()))
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}
