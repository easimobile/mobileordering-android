package com.easipos.mobileordering.activities.reset_password.mvp

import android.app.Application
import com.easipos.mobileordering.R
import com.easipos.mobileordering.base.Presenter
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.orhanobut.logger.Logger

abstract class ResetPasswordBasePresenter(private val application: Application)
    : Presenter<ResetPasswordView>() {

    fun doCheckEmail(email: String) {
        view?.setLoadingIndicator(true)
        FirebaseAuth.getInstance().fetchSignInMethodsForEmail(email)
            .addOnCompleteListener { task ->
                view?.setLoadingIndicator(false)
                if (task.isSuccessful) {
                   Logger.d(task.result?.signInMethods)
                    task.result?.signInMethods?.let { signInMethods ->
                        when {
                            signInMethods.isEmpty() -> {
                                view?.showErrorAlertDialog(application.getString(R.string.error_email_not_exist))
                            }

                            signInMethods.contains(FacebookAuthProvider.FACEBOOK_SIGN_IN_METHOD) -> {
                                view?.showErrorAlertDialog(application.getString(R.string.error_exist_as_facebook_login))
                            }

                            signInMethods.contains(GoogleAuthProvider.GOOGLE_SIGN_IN_METHOD) -> {
                                view?.showErrorAlertDialog(application.getString(R.string.error_exist_as_google_login))
                            }

                            signInMethods.contains(EmailAuthProvider.EMAIL_PASSWORD_SIGN_IN_METHOD) -> {
                                doResetPassword(email)
                            }

                            else -> {
                            }
                        }
                    }
                } else {
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    private fun doResetPassword(email: String) {
        view?.setLoadingIndicator(true)
        FirebaseAuth.getInstance().sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                view?.setLoadingIndicator(false)
                if (task.isSuccessful) {
                    view?.finishScreen()
                    view?.toastMessage(R.string.prompt_reset_password_email_sent)
                } else {
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }
}
