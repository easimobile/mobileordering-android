package com.easipos.mobileordering.activities.splash.navigation

import android.app.Activity
import android.view.View
import com.easipos.mobileordering.activities.login.LoginActivity
import kotlinx.android.extensions.LayoutContainer

abstract class SplashBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, SplashNavigation {

    override fun navigateToLogin(activity: Activity) {
        activity.startActivity(LoginActivity.newIntent(activity))
        activity.finishAffinity()
    }
}