package com.easipos.mobileordering.activities.register.mvp

import com.easipos.mobileordering.base.View

interface RegisterBaseView : View {

    fun navigateToMain()
}
