package com.easipos.mobileordering.activities.splash.mvp

import com.easipos.mobileordering.base.View

interface SplashBaseView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
