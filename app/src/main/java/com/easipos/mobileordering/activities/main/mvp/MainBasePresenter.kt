package com.easipos.mobileordering.activities.main.mvp

import android.app.Application
import com.easipos.mobileordering.base.Presenter
import com.easipos.mobileordering.use_cases.ordering.*

abstract class MainBasePresenter(private val application: Application,
                                 private val getMenuUseCase: GetMenuUseCase,
                                 private val getNewProductsUseCase: GetNewProductsUseCase,
                                 private val insertCartItemUseCase: InsertCartItemUseCase,
                                 private val updateCartItemQuantityUseCase: UpdateCartItemQuantityUseCase,
                                 private val removeCartItemUseCase: RemoveCartItemUseCase,
                                 private val getOrderItemsUseCase: GetOrderItemsUseCase,
                                 private val voidItemUseCase: VoidItemUseCase,
                                 private val getRemarksUseCase: GetRemarksUseCase,
                                 private val submitRemarksUseCase: SubmitRemarksUseCase,
                                 private val holdBillUseCase: HoldBillUseCase,
                                 private val guestCheckUseCase: GetGuestCheckUseCase)
    : Presenter<MainView>() {

    override fun onDetachView() {
        super.onDetachView()
        getMenuUseCase.dispose()
        getNewProductsUseCase.dispose()
        insertCartItemUseCase.dispose()
        updateCartItemQuantityUseCase.dispose()
        removeCartItemUseCase.dispose()
        getOrderItemsUseCase.dispose()
        voidItemUseCase.dispose()
        getRemarksUseCase.dispose()
        submitRemarksUseCase.dispose()
        holdBillUseCase.dispose()
        guestCheckUseCase.dispose()
    }
}
