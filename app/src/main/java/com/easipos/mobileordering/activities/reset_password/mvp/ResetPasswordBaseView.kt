package com.easipos.mobileordering.activities.reset_password.mvp

import com.easipos.mobileordering.base.View

interface ResetPasswordBaseView : View {

    fun finishScreen()
}
