package com.easipos.mobileordering.activities.login.mvp

import com.easipos.mobileordering.api.requests.auth.LoginRequestModel
import com.easipos.mobileordering.api.requests.auth.VerifyUserRequestModel
import com.easipos.mobileordering.base.Presenter
import com.easipos.mobileordering.managers.UserManager
import com.easipos.mobileordering.models.Auth
import com.easipos.mobileordering.models.LoginInfo
import com.easipos.mobileordering.tools.Preference
import com.easipos.mobileordering.use_cases.auth.LoginUseCase
import com.easipos.mobileordering.use_cases.auth.VerifyUserUseCase
import com.easipos.mobileordering.use_cases.base.DefaultSingleObserver
import com.easipos.mobileordering.util.ErrorUtil
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.orhanobut.logger.Logger

abstract class LoginBasePresenter(private val verifyUserUseCase: VerifyUserUseCase,
                                  private val loginUseCase: LoginUseCase)
    : Presenter<LoginView>() {

    override fun onDetachView() {
        super.onDetachView()
        verifyUserUseCase.dispose()
        loginUseCase.dispose()
    }

    fun doFirebaseSignIn(credential: AuthCredential, authType: String) {
        view?.setLoadingIndicator(true)
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    getIdToken { idToken, user ->
                        doVerifyUser(idToken, user, authType)
                    }
                } else {
                    view?.setLoadingIndicator(false)
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    fun doPasswordAuth(email: String, password: String) {
        view?.setLoadingIndicator(true)
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    getIdToken { idToken, user ->
                        doLogin(idToken, user)
                    }
                } else {
                    view?.setLoadingIndicator(false)
                    task.exception?.printStackTrace()
                    view?.showErrorAlertDialog(task.exception?.localizedMessage ?: "")
                }
            }
    }

    private fun getIdToken(action: (String, FirebaseUser) -> Unit) {
        FirebaseAuth.getInstance().currentUser?.apply {
            this.getIdToken(true).addOnCompleteListener {
                it.result?.token?.let { idToken ->
                    Logger.d(idToken)
                    action(idToken, this)
                } ?: run {
                    view?.setLoadingIndicator(false)
                }
            }
        }
    }

    private fun doVerifyUser(idToken: String, firebaseUser: FirebaseUser, authType: String) {
        val model = VerifyUserRequestModel(firebaseUser.uid)
        verifyUserUseCase.execute(object : DefaultSingleObserver<Boolean>() {
            override fun onSuccess(value: Boolean) {
                if (value) {
                    // User existed in db
                    doLogin(idToken, firebaseUser)
                } else {
                    // User not exist in db
                    view?.setLoadingIndicator(false)
                    view?.navigateToRegister(firebaseUser, authType)
                }
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, VerifyUserUseCase.Params.createQuery(model))
    }

    fun doLogin(idToken: String, firebaseUser: FirebaseUser) {
        val model = LoginRequestModel(idToken, firebaseUser.uid)
        view?.setLoadingIndicator(true)
        loginUseCase.execute(object : DefaultSingleObserver<LoginInfo>() {
            override fun onSuccess(value: LoginInfo) {
                view?.setLoadingIndicator(false)
                UserManager.token = Auth(value.token)
                Preference.prefCurrency = value.currency
                view?.navigateToMain()
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error))
            }
        }, LoginUseCase.Params.createQuery(model))
    }
}
