package com.easipos.mobileordering.activities.edit_profile

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.widget.TextView
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.edit_profile.mvp.EditProfilePresenter
import com.easipos.mobileordering.activities.edit_profile.mvp.EditProfileView
import com.easipos.mobileordering.activities.edit_profile.navigation.EditProfileNavigation
import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import com.easipos.mobileordering.models.UserInfo
import com.easipos.mobileordering.util.ImageUtil
import com.easipos.mobileordering.util.loadImageBase64
import com.esafirm.imagepicker.features.ImagePicker
import com.orhanobut.logger.Logger
import com.yalantis.ucrop.UCrop
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import kotlinx.android.synthetic.main.activity_edit_profile.*
import org.jetbrains.anko.longToast
import java.io.File
import javax.inject.Inject

abstract class EditProfileBaseActivity : CustomBaseAppCompatActivity(), EditProfileView {

    //region Variables
    @Inject
    lateinit var presenter: EditProfilePresenter

    @Inject
    lateinit var navigation: EditProfileNavigation

    @Inject
    lateinit var validator: Validator

    private var avatar: Bitmap? = null

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptEditProfile()
        true
    }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            ImagePicker.getFirstImageOrNull(data)?.let { image ->
                cropAvatar(image.path)
            }
        } else if (requestCode == UCrop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                UCrop.getOutput(data)?.let { uri ->
                    setAvatar(uri)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
        DaggerActivityComponent.builder()
            .appComponent((application as Easi).getAppComponent())
            .activityModule(ActivityModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.activity_edit_profile

    override fun init() {
        setupListeners()
        getUserInfo()
    }
    //endregion

    //region LoginView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun populateUserInfo(userInfo: UserInfo) {
        userInfo.profilePictureInBase64?.takeIf {
            it.isNotBlank()
        }?.let { imageStr ->
            image_view_avatar.loadImageBase64(imageStr)
        }

        text_input_edit_text_name.setText(userInfo.name)
        text_input_edit_text_email.setText(userInfo.email)
        text_input_edit_text_phone_number.setText(userInfo.phoneNumber)
    }

    override fun refreshUserInfo() {
        setResult(Activity.RESULT_OK)
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action methods
    private fun getUserInfo() {
        presenter.getUserInfo()
    }

    private fun setupListeners() {
        image_view_back.setOnClickListener {
            finish()
        }

        image_view_avatar.setOnClickListener {
            selectAvatar()
        }

        text_view_change_profile_picture.setOnClickListener {
            selectAvatar()
        }

        button_update.setOnClickListener {
            attemptEditProfile()
        }
    }

    private fun selectAvatar() {
        ImagePicker.create(this)
            .includeVideo(false)
            .includeAnimation(false)
            .single()
            .showCamera(true)
            .start()
    }

    private fun cropAvatar(path: String) {
        Logger.d(path)
        val sourceUri = Uri.fromFile(File(path))
        val destinationUri = Uri.fromFile(File(cacheDir, "avatar.jpg"))
        UCrop.of(sourceUri, destinationUri)
            .withAspectRatio(1f, 1f)
            .withMaxResultSize(500, 500)
            .withOptions(UCrop.Options().apply {
                this.setCircleDimmedLayer(true)
            })
            .start(this)
    }

    @Suppress("DEPRECATION")
    private fun setAvatar(uri: Uri) {
        try {
            avatar = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val source = ImageDecoder.createSource(contentResolver, uri)
                ImageDecoder.decodeBitmap(source)
            } else {
                MediaStore.Images.Media.getBitmap(contentResolver, uri)
            }
            image_view_avatar.setImageBitmap(avatar)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun attemptEditProfile() {
        val nameValidation = Validation(text_input_layout_name)
            .add(NotBlankRule(R.string.error_field_required))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val name = values[0].trim()

                editProfile(name)
            }
        }).validate(nameValidation)
    }

    private fun editProfile(name: String) {
        var avatarInBase64: String? = null
        if (avatar != null) {
            avatarInBase64 = ImageUtil.convert(avatar!!)
        }

        val model = UpdateProfileRequestModel(
            name = name,
            profilePictureInBase64 = avatarInBase64
        )
        presenter.updateProfile(model)
    }
}
