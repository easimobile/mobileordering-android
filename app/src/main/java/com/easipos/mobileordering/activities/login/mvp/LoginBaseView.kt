package com.easipos.mobileordering.activities.login.mvp

import com.easipos.mobileordering.base.View
import com.easipos.mobileordering.constant.AuthType
import com.google.firebase.auth.FirebaseUser

interface LoginBaseView : View {

    fun navigateToRegister(firebaseUser: FirebaseUser? = null, authType: String = AuthType.PASSWORD)

    fun navigateToMain()
}
