package com.easipos.mobileordering.activities.reset_password

import android.widget.TextView
import com.easipos.mobileordering.Easi
import com.easipos.mobileordering.R
import com.easipos.mobileordering.activities.reset_password.mvp.ResetPasswordPresenter
import com.easipos.mobileordering.activities.reset_password.mvp.ResetPasswordView
import com.easipos.mobileordering.activities.reset_password.navigation.ResetPasswordNavigation
import com.easipos.mobileordering.base.CustomBaseAppCompatActivity
import com.easipos.mobileordering.di.components.ActivityComponent
import com.easipos.mobileordering.di.components.DaggerActivityComponent
import com.easipos.mobileordering.di.modules.ActivityModule
import com.easipos.mobileordering.fragments.alert_dialog.CommonAlertDialog
import io.github.anderscheow.validator.Validation
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.rules.common.NotBlankRule
import io.github.anderscheow.validator.rules.regex.EmailRule
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

abstract class ResetPasswordBaseActivity : CustomBaseAppCompatActivity(), ResetPasswordView {

    //region Variables
    @Inject
    lateinit var presenter: ResetPasswordPresenter

    @Inject
    lateinit var navigation: ResetPasswordNavigation

    @Inject
    lateinit var validator: Validator

    private val onEditorActionListener = TextView.OnEditorActionListener { _, _, _ ->
        attemptResetPassword()
        true
    }
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun instantiateComponent(): ActivityComponent =
        DaggerActivityComponent.builder()
            .appComponent((application as Easi).getAppComponent())
            .activityModule(ActivityModule(this))
            .build()

    override fun getResLayout(): Int = R.layout.activity_reset_password

    override fun init() {
        setupListeners()
    }
    //endregion

    //region LoginView Abstract Methods
    override fun toastMessage(message: CharSequence) {
        longToast(message)
    }

    override fun toastMessage(message: Int) {
        longToast(message)
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        CommonAlertDialog(this, message, action).show()
    }

    override fun finishScreen() {
        finish()
    }
    //endregion

    //region Action methods
    private fun setupListeners() {
        text_input_edit_text_email.setOnEditorActionListener(onEditorActionListener)

        image_view_back.setOnClickListener {
            finish()
        }

        button_reset.setOnClickListener {
            attemptResetPassword()
        }
    }

    private fun attemptResetPassword() {
        val emailValidation = Validation(text_input_layout_email)
            .add(NotBlankRule(R.string.error_field_required))
            .add(EmailRule(R.string.error_invalid_email_format))

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val email = values[0].trim()

                presenter.doCheckEmail(email)
            }
        }).validate(emailValidation)
    }
}
