package com.easipos.mobileordering.activities.reset_password.navigation

import android.view.View
import kotlinx.android.extensions.LayoutContainer

abstract class ResetPasswordBaseNavigationImpl(override val containerView: View)
    : LayoutContainer, ResetPasswordNavigation