package com.easipos.mobileordering.activities.login.navigation

import android.app.Activity
import com.easipos.mobileordering.constant.AuthType
import com.google.firebase.auth.FirebaseUser

interface LoginBaseNavigation {

    fun navigateToRegister(activity: Activity, firebaseUser: FirebaseUser? = null, authType: String = AuthType.PASSWORD)

    fun navigateToForgotPassword(activity: Activity)

    fun navigateToMain(activity: Activity)
}