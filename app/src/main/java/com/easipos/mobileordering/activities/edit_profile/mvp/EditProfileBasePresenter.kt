package com.easipos.mobileordering.activities.edit_profile.mvp

import com.easipos.mobileordering.R
import com.easipos.mobileordering.api.requests.user.UpdateProfileRequestModel
import com.easipos.mobileordering.base.Presenter
import com.easipos.mobileordering.models.UserInfo
import com.easipos.mobileordering.use_cases.base.DefaultObserver
import com.easipos.mobileordering.use_cases.base.DefaultSingleObserver
import com.easipos.mobileordering.use_cases.user.GetUserInfoUseCase
import com.easipos.mobileordering.use_cases.user.UpdateProfileUseCase
import com.easipos.mobileordering.util.ErrorUtil

abstract class EditProfileBasePresenter(private val getUserInfoUseCase: GetUserInfoUseCase,
                                        private val updateProfileUseCase: UpdateProfileUseCase
)
    : Presenter<EditProfileView>() {

    override fun onDetachView() {
        super.onDetachView()
        getUserInfoUseCase.dispose()
        updateProfileUseCase.dispose()
    }

    fun getUserInfo() {
        view?.setLoadingIndicator(true)
        getUserInfoUseCase.execute(object : DefaultSingleObserver<UserInfo>() {
            override fun onSuccess(value: UserInfo) {
                view?.setLoadingIndicator(false)
                view?.populateUserInfo(value)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error)) {
                    view?.finishScreen()
                }
            }
        }, GetUserInfoUseCase.Params.createQuery())
    }

    fun updateProfile(model: UpdateProfileRequestModel) {
        view?.setLoadingIndicator(true)
        updateProfileUseCase.execute(object : DefaultObserver<Void>() {
            override fun onComplete() {
                view?.setLoadingIndicator(false)
                view?.refreshUserInfo()
                view?.finishScreen()
                view?.toastMessage(R.string.prompt_update_profile_successful)
            }

            override fun onError(error: Throwable) {
                super.onError(error)
                view?.setLoadingIndicator(false)
                view?.showErrorAlertDialog(ErrorUtil.parseException(error)) {
                    view?.finishScreen()
                }
            }
        }, UpdateProfileUseCase.Params.createQuery(model))
    }
}
