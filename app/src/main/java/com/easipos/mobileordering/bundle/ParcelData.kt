package com.easipos.mobileordering.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val FIREBASE_USER = "firebase_user"
    const val AUTH_TYPE = "auth_type"
    const val CART_ITEM_ID = "CART_ITEM_ID"
    const val PROD_CODE = "PROD_CODE"
    const val REMARKS = "REMARKS"
    const val RECEIPT = "RECEIPT"
}