package com.easipos.mobileordering.bundle

object RequestCode {

    const val FINISH_LOGIN_RC = 1001
    const val GOOGLE_SIGN_IN_RC = 1002
}