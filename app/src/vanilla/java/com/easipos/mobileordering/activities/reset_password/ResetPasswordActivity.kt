package com.easipos.mobileordering.activities.reset_password

import android.content.Context
import android.content.Intent

class ResetPasswordActivity : ResetPasswordBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ResetPasswordActivity::class.java)
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}