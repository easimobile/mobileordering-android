package com.easipos.mobileordering.activities.splash.navigation

import android.view.View

class SplashNavigationImpl(override val containerView: View)
    : SplashBaseNavigationImpl(containerView)