package com.easipos.mobileordering.activities.edit_profile.navigation

import android.view.View

class EditProfileNavigationImpl(override val containerView: View)
    : EditProfileBaseNavigationImpl(containerView)