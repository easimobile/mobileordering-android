package com.easipos.mobileordering.activities.login.navigation

import android.view.View

class LoginNavigationImpl(override val containerView: View)
    : LoginBaseNavigationImpl(containerView)