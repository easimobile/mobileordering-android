package com.easipos.mobileordering.activities.change_password.navigation

import android.view.View

class ChangePasswordNavigationImpl(override val containerView: View)
    : ChangePasswordBaseNavigationImpl(containerView)