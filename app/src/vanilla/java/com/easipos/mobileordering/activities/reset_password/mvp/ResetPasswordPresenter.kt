package com.easipos.mobileordering.activities.reset_password.mvp

import android.app.Application
import javax.inject.Inject

class ResetPasswordPresenter @Inject constructor(application: Application)
    : ResetPasswordBasePresenter(application)