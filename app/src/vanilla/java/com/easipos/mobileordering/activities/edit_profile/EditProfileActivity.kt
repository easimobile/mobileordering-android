package com.easipos.mobileordering.activities.edit_profile

import android.content.Context
import android.content.Intent

class EditProfileActivity : EditProfileBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, EditProfileBaseActivity::class.java)
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}
