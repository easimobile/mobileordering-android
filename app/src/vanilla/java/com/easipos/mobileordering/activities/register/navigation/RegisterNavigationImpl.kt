package com.easipos.mobileordering.activities.register.navigation

import android.view.View

class RegisterNavigationImpl(override val containerView: View)
    : RegisterBaseNavigationImpl(containerView)