package com.easipos.mobileordering.activities.change_password

import android.content.Context
import android.content.Intent

class ChangePasswordActivity : ChangePasswordBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, ChangePasswordBaseActivity::class.java)
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}
