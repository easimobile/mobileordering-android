package com.easipos.mobileordering.activities.main

import android.content.Context
import android.content.Intent

class MainActivity : MainBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}
