package com.easipos.mobileordering.activities.register.mvp

import com.easipos.mobileordering.use_cases.auth.RegisterUseCase
import javax.inject.Inject

class RegisterPresenter @Inject constructor(registerUseCase: RegisterUseCase)
    : RegisterBasePresenter(registerUseCase)
