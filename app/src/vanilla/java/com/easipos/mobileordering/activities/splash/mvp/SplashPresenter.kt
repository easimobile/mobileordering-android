package com.easipos.mobileordering.activities.splash.mvp

import com.easipos.mobileordering.use_cases.precheck.CheckVersionUseCase
import javax.inject.Inject

class SplashPresenter @Inject constructor(checkVersionUseCase: CheckVersionUseCase)
    : SplashBasePresenter(checkVersionUseCase)
