package com.easipos.mobileordering.activities.register

import android.content.Context
import android.content.Intent
import com.easipos.mobileordering.bundle.ParcelData
import com.easipos.mobileordering.constant.AuthType
import com.google.firebase.auth.FirebaseUser

class RegisterActivity : RegisterBaseActivity() {

    companion object {
        fun newIntent(context: Context, firebaseUser: FirebaseUser? = null, authType: String = AuthType.PASSWORD): Intent {
            return Intent(context, RegisterActivity::class.java).apply {
                this.putExtra(ParcelData.FIREBASE_USER, firebaseUser)
                this.putExtra(ParcelData.AUTH_TYPE, authType)
            }
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}
