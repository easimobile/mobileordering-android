package com.easipos.mobileordering.activities.main.mvp

import android.app.Application
import com.easipos.mobileordering.use_cases.ordering.*
import javax.inject.Inject

class MainPresenter @Inject constructor(application: Application,
                                        getMenuUseCase: GetMenuUseCase,
                                        getNewProductsUseCase: GetNewProductsUseCase,
                                        insertCartItemUseCase: InsertCartItemUseCase,
                                        updateCartItemQuantityUseCase: UpdateCartItemQuantityUseCase,
                                        removeCartItemUseCase: RemoveCartItemUseCase,
                                        getOrderItemsUseCase: GetOrderItemsUseCase,
                                        voidItemUseCase: VoidItemUseCase,
                                        getRemarksUseCase: GetRemarksUseCase,
                                        submitRemarksUseCase: SubmitRemarksUseCase,
                                        holdBillUseCase: HoldBillUseCase,
                                        guestCheckUseCase: GetGuestCheckUseCase)
    : MainBasePresenter(application, getMenuUseCase, getNewProductsUseCase, insertCartItemUseCase, 
    updateCartItemQuantityUseCase, removeCartItemUseCase, getOrderItemsUseCase, 
    voidItemUseCase, getRemarksUseCase, submitRemarksUseCase, holdBillUseCase, guestCheckUseCase)
