package com.easipos.mobileordering.activities.reset_password.navigation

import android.view.View

class ResetPasswordNavigationImpl(override val containerView: View)
    : ResetPasswordBaseNavigationImpl(containerView)