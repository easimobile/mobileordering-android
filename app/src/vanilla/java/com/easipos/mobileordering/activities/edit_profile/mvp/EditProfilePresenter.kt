package com.easipos.mobileordering.activities.edit_profile.mvp

import com.easipos.mobileordering.use_cases.user.GetUserInfoUseCase
import com.easipos.mobileordering.use_cases.user.UpdateProfileUseCase
import javax.inject.Inject

class EditProfilePresenter @Inject constructor(getUserInfoUseCase: GetUserInfoUseCase,
                                               updateProfileUseCase: UpdateProfileUseCase
)
    : EditProfileBasePresenter(getUserInfoUseCase, updateProfileUseCase)
