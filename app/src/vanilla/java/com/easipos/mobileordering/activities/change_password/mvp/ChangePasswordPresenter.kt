package com.easipos.mobileordering.activities.change_password.mvp

import android.app.Application
import javax.inject.Inject

class ChangePasswordPresenter @Inject constructor(application: Application)
    : ChangePasswordBasePresenter(application)
