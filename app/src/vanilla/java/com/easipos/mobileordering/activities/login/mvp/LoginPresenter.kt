package com.easipos.mobileordering.activities.login.mvp

import com.easipos.mobileordering.use_cases.auth.LoginUseCase
import com.easipos.mobileordering.use_cases.auth.VerifyUserUseCase
import javax.inject.Inject

class LoginPresenter @Inject constructor(verifyUserUseCase: VerifyUserUseCase,
                                         loginUseCase: LoginUseCase)
    : LoginBasePresenter(verifyUserUseCase, loginUseCase)
