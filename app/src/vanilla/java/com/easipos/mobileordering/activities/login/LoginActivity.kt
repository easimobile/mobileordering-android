package com.easipos.mobileordering.activities.login

import android.content.Context
import android.content.Intent

class LoginActivity : LoginBaseActivity() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun initInjection() {
        super.initInjection()
        component?.inject(this)
        presenter.onAttachView(this)
    }
}
